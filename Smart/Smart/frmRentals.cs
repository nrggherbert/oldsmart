﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WIA;
using System.IO;
using Microsoft.Office.Interop.Outlook;

namespace Smart
{
    public partial class frmRentals : Form
    {

        DataTable dt = new DataTable();
        SqlCommand cmd = new SqlCommand();
        SqlConnection con = new SqlConnection();
        public frmRentals()
        {
            InitializeComponent();
        }

        private void frmRentals_Load(object sender, EventArgs e)
        {
            this.usp_SMART_getVehiclesForCustomerTableAdapter.Fill(this.ramsdevDataSet3.usp_SMART_getVehiclesForCustomer);
            this.usp_SMART_getCustomerGroupsTableAdapter.Fill(this.ramsdevDataSet2.usp_SMART_getCustomerGroups);
            cboCustomer.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cboCustomer.AutoCompleteSource = AutoCompleteSource.ListItems;
            cboVehicle.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cboVehicle.AutoCompleteSource = AutoCompleteSource.ListItems;

            // Scanning ***************************************************************************************************
            // Create a DeviceManager instance
            var deviceManager = new DeviceManager();

            // Loop through the list of devices
            for (int i = 1; i <= deviceManager.DeviceInfos.Count; i++)
            {
                // Skip the device if it's not a scanner
                if ((deviceManager.DeviceInfos[i].Type != WiaDeviceType.ScannerDeviceType) && (deviceManager.DeviceInfos[i].Type != WiaDeviceType.CameraDeviceType))
                {
                    continue;
                }
                listBox1.Items.Add(deviceManager.DeviceInfos[i].Properties["Name"].get_Value());

                // Print something like e.g "WIA Canoscan 4400F"
                //                Console.WriteLine(deviceManager.DeviceInfos[i].Properties["Name"].get_Value());
                // e.g Canoscan 4400F
                //Console.WriteLine(deviceManager.DeviceInfos[i].Properties["Description"].get_Value());
                // e.g \\.\Usbscan0
                //Console.WriteLine(deviceManager.DeviceInfos[i].Properties["Port"].get_Value());
            }

            // Scanning ***************************************************************************************************

        }

        private void cboCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*
            cboVehicle.DataSource = null;
            cboVehicle.Items.Clear();
            SqlDataAdapter dar = new SqlDataAdapter(cmd);
            string SQL;
            con = new SqlConnection(Properties.Settings.Default.ramsdevConnectionString);
            con.Open();
            cmd.Connection = con;
            SQL = "SELECT FleetID, Customer_Fleetlist_VehicleReg, Customer_GroupID FROM Customer_Fleetlist WHERE Customer_GroupID = " + cboCustomer.SelectedValue.ToString();
            cmd.CommandText = SQL;
            dar.Fill(dt);
            cboVehicle.DataSource = dt;
            */

        }

        private void dataGridView1_Click(object sender, EventArgs e)
        {

        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            this.usp_SMART_rentalAvailabilityTableAdapter.Fill(this.smartdevDataSet.usp_SMART_rentalAvailability);
            Cursor.Current = Cursors.Default;

        }

        private void button1_Click(object sender, EventArgs e)
        {

            // Create a DeviceManager instance
            var deviceManager = new DeviceManager();

            // Create an empty variable to store the scanner instance
            DeviceInfo firstScannerAvailable = null;

            // Loop through the list of devices to choose the first available
            for (int i = 1; i <= deviceManager.DeviceInfos.Count; i++)
            {
                // Skip the device if it's not a scanner
                if (deviceManager.DeviceInfos[i].Type != WiaDeviceType.ScannerDeviceType)
                {
                    continue;
                }

                firstScannerAvailable = deviceManager.DeviceInfos[i];

                break;
            }
            // Connect to the first available scanner
            var device = firstScannerAvailable.Connect();

            // Select the scanner
            var scannerItem = device.Items[1];

            // Retrieve a image in JPEG format and store it into a variable
            //            var imageFile = (ImageFile)scannerItem.Transfer(FormatID.wiaFormatJPEG);

            CommonDialogClass dlg = new CommonDialogClass();
            object scanResult = dlg.ShowTransfer(scannerItem, WIA.FormatID.wiaFormatPNG, true);

            if (scanResult != null)
            {
                ImageFile image = (ImageFile)scanResult;

                // Do the rest of things as save the image 

                // Save the image in some path with filename
                var path = @"C:\smart\scan.jpeg";

                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                // Save image !
                image.SaveFile(path);
            }
        }

        private void tabPage1_DragDrop(object sender, DragEventArgs e)
        {
        }

        private void panel2_DragDrop(object sender, DragEventArgs e)
        {
            string strFilename = null;

            //something about the act of reading this stream creates the file in your temp folder(?)
            using (MemoryStream stream = (MemoryStream)e.Data.GetData("attachment format", true))
            {
                byte[] b = new byte[stream.Length];
                stream.Read(b, 0, (int)stream.Length);
                strFilename = Encoding.Unicode.GetString(b);
                //The path/filename is at position 10.
                strFilename = strFilename.Substring(10, strFilename.IndexOf('\0', 10) - 10);
                stream.Close();
            }

            if (strFilename != null && File.Exists(strFilename))
            {
                //From here on out, you're just reading another file from the disk...
                using (FileStream fileIn = File.Open(strFilename, FileMode.Open))
                {
                    fileIn.Close();
                }
            }

            File.Delete(strFilename);

        }

        private void button2_Click(object sender, EventArgs e)
        {
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
        }

        private void panel2_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
        }

        private void panel2_DragDrop_1(object sender, DragEventArgs e)
        {

            string[] fileNames = null;


            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                fileNames = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string fileName in fileNames)
                {
                }
            }
            else if (e.Data.GetDataPresent("FileGroupDescriptor"))
            {
                object s = e.Data.GetData("FileGroupDescriptor");

                Stream theStream = (Stream)e.Data.GetData("FileGroupDescriptor");
                byte[] fileGroupDescriptor = new byte[512];
                theStream.Read(fileGroupDescriptor, 0, 512);

                StringBuilder fileName = new StringBuilder("");

                for (int i = 76; fileGroupDescriptor[i] != 0; i++)
                { fileName.Append(Convert.ToChar(fileGroupDescriptor[i])); }

                string theFile = fileName.ToString();
                String fileName1 = System.IO.Path.GetFileName(theFile);

                // get the actual raw file into memory
                MemoryStream ms = (MemoryStream)e.Data.GetData("FileContents", true);
                // allocate enough bytes to hold the raw data
                byte[] fileBytes = new byte[ms.Length];
                // set starting position at first byte and read in the raw data
                ms.Position = 0;
                ms.Read(fileBytes, 0, (int)ms.Length);
                // create a file and save the raw zip file to it
                FileStream fs = new FileStream(theFile, FileMode.Create);
                fs.Write(fileBytes, 0, (int)fileBytes.Length);

                fs.Close();  // close the file

                FileInfo tempFile = new FileInfo(theFile);

                // always good to make sure we actually created the file
                if (tempFile.Exists == true)
                {
                    // for now, just delete what we created
                    tempFile.Delete();
                }
                else
                {
                //    Trace.WriteLine("File was not created!");
                }
                /*

                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string filePath in files)
            {

                listBox2.Items.Add(filePath);
            }
            */
            }
        }
    }
}
