﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;
namespace Smart
{
    public partial class frmAdmin : Form
    {
        public frmAdmin()
        {
            InitializeComponent();
        }

        private void frmAdmin_Load(object sender, EventArgs e)
        {

        }

        private void tcAdmin_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcAdmin.SelectedTab.Name == "tpCustomers")
            {
                BindingSource bsGroups = new BindingSource();
                BindingSource bsLocs = new BindingSource();
                dgvGroups.DataSource = bsGroups;
                dgvLocs.DataSource = bsLocs;


                String conString = "Data Source=62.232.42.85;Initial Catalog=ramsdev;Persist Security Info=True;User ID=tamsuser;Password=sql";
                SqlConnection con = new SqlConnection(conString);
                DataSet ds = new DataSet();

                SqlDataAdapter daGroups = new SqlDataAdapter("SELECT * FROM Customer_Group", con);
                daGroups.Fill(ds, "Customer_Group");

                SqlDataAdapter daLocations = new SqlDataAdapter("SELECT * FROM Customer_Location", con);
                daLocations.Fill(ds, "Customer_Location");

                DataRelation dr = new DataRelation("Customers", ds.Tables["Customer_Group"].Columns["Customer_GroupID"],
                                        ds.Tables["Customer_Location"].Columns["Customer_GroupID"]);
                ds.Relations.Add(dr);

                bsGroups.DataSource = ds;
                bsGroups.DataMember = "Customer_Group";

                bsLocs.DataSource = bsGroups;
                bsLocs.DataMember = "Customers";

                dgvGroups.AutoResizeColumns();
                dgvLocs.AutoResizeColumns();

            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
        }

        private void miExport_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            pbExport.Maximum = dgvGroups.RowCount;
            pbExport.Step = 1;
            pbExport.Value = 0;
            pbExport.Visible = true;
            pbExport.BringToFront();

//            BackgroundWorker bw = new BackgroundWorker();
            //bw.RunWorkerAsync();

            string csv = string.Empty;

            foreach (DataGridViewColumn column in dgvGroups.Columns)
            {
                csv += column.HeaderText + ',';
            }

            csv += "\r\n";

            foreach (DataGridViewRow row in dgvGroups.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    csv += cell.Value.ToString().Replace(",", ";") + ',';
                }

                csv += "\r\n";
                pbExport.Increment(1);
            }

            string folderPath = @"c:\smart\export.csv";
            File.WriteAllText(folderPath, csv);
            pbExport.Visible = false;
            Cursor.Current = Cursors.Default;

            System.Diagnostics.Process.Start(@"c:\smart\export.csv");
        }
    }
}

