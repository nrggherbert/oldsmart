﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Smart
{
    public partial class frmStock : Form
    {
        public frmStock()
        {
            InitializeComponent();
        }

        private void frmStock_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'uspSMARTstockHistoryBindingSource.usp_SMART_stockHistory' table. You can move, or remove it, as needed.
            this.usp_SMART_stockHistoryTableAdapter1.Fill(this.uspSMARTstockHistoryBindingSource.usp_SMART_stockHistory);
            // TODO: This line of code loads data into the 'smartdevDataSet1.usp_SMART_getCostCentres' table. You can move, or remove it, as needed.
            this.usp_SMART_getCostCentresTableAdapter.Fill(this.smartdevDataSet1.usp_SMART_getCostCentres);

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tpHistory)
            {
                Cursor.Current = Cursors.WaitCursor;
                this.usp_SMART_stockHistoryTableAdapter.Fill(this.smartdevDataSet2.usp_SMART_stockHistory);
                Cursor.Current = Cursors.Default;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbTyreSizeSearch.Clear();
            if (tabControl1.SelectedTab == tpHistory)
            {
                if (cboCostCentre.Text == "ALL")
                {
                    uspSMARTstockHistoryBindingSource1.Filter = "";
                }
                else
                {
                    uspSMARTstockHistoryBindingSource1.Filter = "Centre_Desc = '" + cboCostCentre.Text + "'";
                }
            }
            else
            {
                if (cboCostCentre.Text == "ALL")
                {
                    //uspSMARTgetCurrentStockBindingSource.DataSource.par
                }
                else
                {
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string filter;
            if (cboCostCentre.Text != "ALL")
            {
                filter = "Centre_Desc = '" + cboCostCentre.Text + "' AND ";
            }
            else
            {
                filter = "";
                //uspSMARTstockHistoryBindingSource.Filter = uspSMARTstockHistoryBindingSource.Filter +
            }
            filter = filter + "Product_Size_Short LIKE '%" + tbTyreSizeSearch.Text + "%' ";
            uspSMARTstockHistoryBindingSource1.Filter = filter;
        }
    }
}
