﻿namespace Smart
{
    partial class frmRentals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.cboCustomer = new System.Windows.Forms.ComboBox();
            this.uspSMARTgetCustomerGroupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ramsdevDataSet2 = new Smart.ramsdevDataSet2();
            this.cboVehicle = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uspSMARTgetVehiclesForCustomerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ramsdevDataSet3 = new Smart.ramsdevDataSet3();
            this.ramsdevDataSet1 = new Smart.ramsdevDataSet1();
            this.ramsDev = new Smart.ramsDev();
            this.ramsDevBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usp_SMART_getCustomerGroupsTableAdapter = new Smart.ramsdevDataSet2TableAdapters.usp_SMART_getCustomerGroupsTableAdapter();
            this.usp_SMART_getVehiclesForCustomerTableAdapter = new Smart.ramsdevDataSet3TableAdapters.usp_SMART_getVehiclesForCustomerTableAdapter();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpRentalJob = new System.Windows.Forms.TabPage();
            this.tpAvailability = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleNotesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceAdueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mOTdueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lolerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.durationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uspSMARTrentalAvailabilityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.smartdevDataSet = new Smart.smartdevDataSet();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.usp_SMART_rentalAvailabilityTableAdapter = new Smart.smartdevDataSetTableAdapters.usp_SMART_rentalAvailabilityTableAdapter();
            this.ramsDev1 = new Smart.ramsDev();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCustomerGroupsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetVehiclesForCustomerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsDev)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsDevBindingSource)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tpAvailability.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTrentalAvailabilityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ramsDev1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox4);
            this.panel1.Controls.Add(this.cboCustomer);
            this.panel1.Controls.Add(this.cboVehicle);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(978, 169);
            this.panel1.TabIndex = 0;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Daily",
            "Weekly",
            "Monthly",
            "Annually"});
            this.comboBox4.Location = new System.Drawing.Point(323, 3);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 21);
            this.comboBox4.TabIndex = 11;
            // 
            // cboCustomer
            // 
            this.cboCustomer.DataSource = this.uspSMARTgetCustomerGroupsBindingSource;
            this.cboCustomer.DisplayMember = "Customer_Group_Name";
            this.cboCustomer.FormattingEnabled = true;
            this.cboCustomer.Location = new System.Drawing.Point(507, 5);
            this.cboCustomer.MaxDropDownItems = 20;
            this.cboCustomer.Name = "cboCustomer";
            this.cboCustomer.Size = new System.Drawing.Size(227, 21);
            this.cboCustomer.TabIndex = 10;
            this.cboCustomer.ValueMember = "Customer_GroupID";
            this.cboCustomer.SelectedIndexChanged += new System.EventHandler(this.cboCustomer_SelectedIndexChanged);
            // 
            // uspSMARTgetCustomerGroupsBindingSource
            // 
            this.uspSMARTgetCustomerGroupsBindingSource.DataMember = "usp_SMART_getCustomerGroups";
            this.uspSMARTgetCustomerGroupsBindingSource.DataSource = this.ramsdevDataSet2;
            // 
            // ramsdevDataSet2
            // 
            this.ramsdevDataSet2.DataSetName = "ramsdevDataSet2";
            this.ramsdevDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cboVehicle
            // 
            this.cboVehicle.DisplayMember = "FleetID";
            this.cboVehicle.FormattingEnabled = true;
            this.cboVehicle.Location = new System.Drawing.Point(788, 5);
            this.cboVehicle.Name = "cboVehicle";
            this.cboVehicle.Size = new System.Drawing.Size(121, 21);
            this.cboVehicle.TabIndex = 9;
            this.cboVehicle.ValueMember = "FleetID";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Spot Hire",
            "Contract Hire",
            "Cross Hire"});
            this.comboBox1.Location = new System.Drawing.Point(84, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(740, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Vehicle";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(450, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Customer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(250, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Billing Period";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rental Type";
            // 
            // uspSMARTgetVehiclesForCustomerBindingSource
            // 
            this.uspSMARTgetVehiclesForCustomerBindingSource.DataMember = "usp_SMART_getVehiclesForCustomer";
            this.uspSMARTgetVehiclesForCustomerBindingSource.DataSource = this.ramsdevDataSet3;
            // 
            // ramsdevDataSet3
            // 
            this.ramsdevDataSet3.DataSetName = "ramsdevDataSet3";
            this.ramsdevDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ramsdevDataSet1
            // 
            this.ramsdevDataSet1.DataSetName = "ramsdevDataSet1";
            this.ramsdevDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ramsDev
            // 
            this.ramsDev.DataSetName = "ramsDev";
            this.ramsDev.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ramsDevBindingSource
            // 
            this.ramsDevBindingSource.DataSource = this.ramsDev;
            this.ramsDevBindingSource.Position = 0;
            // 
            // usp_SMART_getCustomerGroupsTableAdapter
            // 
            this.usp_SMART_getCustomerGroupsTableAdapter.ClearBeforeFill = true;
            // 
            // usp_SMART_getVehiclesForCustomerTableAdapter
            // 
            this.usp_SMART_getVehiclesForCustomerTableAdapter.ClearBeforeFill = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpRentalJob);
            this.tabControl1.Controls.Add(this.tpAvailability);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 169);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(978, 368);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tpRentalJob
            // 
            this.tpRentalJob.Location = new System.Drawing.Point(4, 22);
            this.tpRentalJob.Name = "tpRentalJob";
            this.tpRentalJob.Padding = new System.Windows.Forms.Padding(3);
            this.tpRentalJob.Size = new System.Drawing.Size(970, 342);
            this.tpRentalJob.TabIndex = 0;
            this.tpRentalJob.Text = "Rental Job";
            this.tpRentalJob.UseVisualStyleBackColor = true;
            // 
            // tpAvailability
            // 
            this.tpAvailability.Controls.Add(this.dataGridView1);
            this.tpAvailability.Location = new System.Drawing.Point(4, 22);
            this.tpAvailability.Name = "tpAvailability";
            this.tpAvailability.Padding = new System.Windows.Forms.Padding(3);
            this.tpAvailability.Size = new System.Drawing.Size(970, 342);
            this.tpAvailability.TabIndex = 1;
            this.tpAvailability.Text = "Availability";
            this.tpAvailability.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn,
            this.vehicleTypeDataGridViewTextBoxColumn,
            this.vehicleNotesDataGridViewTextBoxColumn,
            this.serviceAdueDataGridViewTextBoxColumn,
            this.mOTdueDataGridViewTextBoxColumn,
            this.lolerDataGridViewTextBoxColumn,
            this.startDataGridViewTextBoxColumn,
            this.durationDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.uspSMARTrentalAvailabilityBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(964, 336);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.Click += new System.EventHandler(this.dataGridView1_Click);
            // 
            // customerFleetlistVehicleRegDataGridViewTextBoxColumn
            // 
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn.DataPropertyName = "Customer_Fleetlist_VehicleReg";
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn.HeaderText = "Customer_Fleetlist_VehicleReg";
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn.Name = "customerFleetlistVehicleRegDataGridViewTextBoxColumn";
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn.ReadOnly = true;
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn.Width = 178;
            // 
            // vehicleTypeDataGridViewTextBoxColumn
            // 
            this.vehicleTypeDataGridViewTextBoxColumn.DataPropertyName = "VehicleType";
            this.vehicleTypeDataGridViewTextBoxColumn.HeaderText = "VehicleType";
            this.vehicleTypeDataGridViewTextBoxColumn.Name = "vehicleTypeDataGridViewTextBoxColumn";
            this.vehicleTypeDataGridViewTextBoxColumn.ReadOnly = true;
            this.vehicleTypeDataGridViewTextBoxColumn.Width = 91;
            // 
            // vehicleNotesDataGridViewTextBoxColumn
            // 
            this.vehicleNotesDataGridViewTextBoxColumn.DataPropertyName = "Vehicle_Notes";
            this.vehicleNotesDataGridViewTextBoxColumn.HeaderText = "Vehicle_Notes";
            this.vehicleNotesDataGridViewTextBoxColumn.Name = "vehicleNotesDataGridViewTextBoxColumn";
            this.vehicleNotesDataGridViewTextBoxColumn.ReadOnly = true;
            this.vehicleNotesDataGridViewTextBoxColumn.Width = 101;
            // 
            // serviceAdueDataGridViewTextBoxColumn
            // 
            this.serviceAdueDataGridViewTextBoxColumn.DataPropertyName = "Service_A_due";
            this.serviceAdueDataGridViewTextBoxColumn.HeaderText = "Service_A_due";
            this.serviceAdueDataGridViewTextBoxColumn.Name = "serviceAdueDataGridViewTextBoxColumn";
            this.serviceAdueDataGridViewTextBoxColumn.ReadOnly = true;
            this.serviceAdueDataGridViewTextBoxColumn.Width = 105;
            // 
            // mOTdueDataGridViewTextBoxColumn
            // 
            this.mOTdueDataGridViewTextBoxColumn.DataPropertyName = "MOT_due";
            this.mOTdueDataGridViewTextBoxColumn.HeaderText = "MOT_due";
            this.mOTdueDataGridViewTextBoxColumn.Name = "mOTdueDataGridViewTextBoxColumn";
            this.mOTdueDataGridViewTextBoxColumn.ReadOnly = true;
            this.mOTdueDataGridViewTextBoxColumn.Width = 80;
            // 
            // lolerDataGridViewTextBoxColumn
            // 
            this.lolerDataGridViewTextBoxColumn.DataPropertyName = "Loler";
            this.lolerDataGridViewTextBoxColumn.HeaderText = "Loler";
            this.lolerDataGridViewTextBoxColumn.Name = "lolerDataGridViewTextBoxColumn";
            this.lolerDataGridViewTextBoxColumn.ReadOnly = true;
            this.lolerDataGridViewTextBoxColumn.Width = 55;
            // 
            // startDataGridViewTextBoxColumn
            // 
            this.startDataGridViewTextBoxColumn.DataPropertyName = "Start";
            this.startDataGridViewTextBoxColumn.HeaderText = "Start";
            this.startDataGridViewTextBoxColumn.Name = "startDataGridViewTextBoxColumn";
            this.startDataGridViewTextBoxColumn.ReadOnly = true;
            this.startDataGridViewTextBoxColumn.Width = 54;
            // 
            // durationDataGridViewTextBoxColumn
            // 
            this.durationDataGridViewTextBoxColumn.DataPropertyName = "Duration";
            this.durationDataGridViewTextBoxColumn.HeaderText = "Duration";
            this.durationDataGridViewTextBoxColumn.Name = "durationDataGridViewTextBoxColumn";
            this.durationDataGridViewTextBoxColumn.ReadOnly = true;
            this.durationDataGridViewTextBoxColumn.Width = 72;
            // 
            // uspSMARTrentalAvailabilityBindingSource
            // 
            this.uspSMARTrentalAvailabilityBindingSource.DataMember = "usp_SMART_rentalAvailability";
            this.uspSMARTrentalAvailabilityBindingSource.DataSource = this.smartdevDataSet;
            // 
            // smartdevDataSet
            // 
            this.smartdevDataSet.DataSetName = "smartdevDataSet";
            this.smartdevDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(970, 342);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Rental Renew";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(970, 342);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Online Bookings";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.button1);
            this.tabPage5.Controls.Add(this.listBox1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(970, 342);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Scan TEST";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(319, 92);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Scan TEST";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(81, 71);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(120, 95);
            this.listBox1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(970, 342);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Reports";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // usp_SMART_rentalAvailabilityTableAdapter
            // 
            this.usp_SMART_rentalAvailabilityTableAdapter.ClearBeforeFill = true;
            // 
            // ramsDev1
            // 
            this.ramsDev1.DataSetName = "ramsDev";
            this.ramsDev1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // frmRentals
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(978, 537);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Name = "frmRentals";
            this.Text = "Rentals";
            this.Load += new System.EventHandler(this.frmRentals_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCustomerGroupsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetVehiclesForCustomerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsDev)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsDevBindingSource)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tpAvailability.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTrentalAvailabilityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ramsDev1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox cboCustomer;
        private System.Windows.Forms.ComboBox cboVehicle;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ramsDev ramsDev;
        private System.Windows.Forms.BindingSource ramsDevBindingSource;
        private ramsdevDataSet1 ramsdevDataSet1;
        private ramsdevDataSet2 ramsdevDataSet2;
        private System.Windows.Forms.BindingSource uspSMARTgetCustomerGroupsBindingSource;
        private ramsdevDataSet2TableAdapters.usp_SMART_getCustomerGroupsTableAdapter usp_SMART_getCustomerGroupsTableAdapter;
        private ramsdevDataSet3 ramsdevDataSet3;
        private System.Windows.Forms.BindingSource uspSMARTgetVehiclesForCustomerBindingSource;
        private ramsdevDataSet3TableAdapters.usp_SMART_getVehiclesForCustomerTableAdapter usp_SMART_getVehiclesForCustomerTableAdapter;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpRentalJob;
        private System.Windows.Forms.TabPage tpAvailability;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dataGridView1;
        private smartdevDataSet smartdevDataSet;
        private System.Windows.Forms.BindingSource uspSMARTrentalAvailabilityBindingSource;
        private smartdevDataSetTableAdapters.usp_SMART_rentalAvailabilityTableAdapter usp_SMART_rentalAvailabilityTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerFleetlistVehicleRegDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehicleNotesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceAdueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn mOTdueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lolerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn durationDataGridViewTextBoxColumn;
        private ramsDev ramsDev1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage1;
    }
}