﻿namespace Smart
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.mniHome = new System.Windows.Forms.ToolStripMenuItem();
            this.mniMaint = new System.Windows.Forms.ToolStripMenuItem();
            this.rentalsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsiCalendar = new System.Windows.Forms.ToolStripMenuItem();
            this.stockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.poundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reoprtsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adminToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calendarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(81, 92);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem2.Text = "1";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem3.Text = "2";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem4.Text = "3";
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(80, 22);
            this.toolStripMenuItem5.Text = "4";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1048, 76);
            this.panel1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.PowderBlue;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1048, 76);
            this.label1.TabIndex = 0;
            this.label1.Text = "SMART";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuMain
            // 
            this.menuMain.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniHome,
            this.mniMaint,
            this.rentalsToolStripMenuItem,
            this.tsiCalendar,
            this.stockToolStripMenuItem,
            this.poundToolStripMenuItem,
            this.reoprtsToolStripMenuItem,
            this.adminToolStripMenuItem,
            this.calendarToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 76);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(68, 450);
            this.menuMain.TabIndex = 2;
            this.menuMain.Text = "menuStrip1";
            // 
            // mniHome
            // 
            this.mniHome.BackColor = System.Drawing.Color.PowderBlue;
            this.mniHome.Image = ((System.Drawing.Image)(resources.GetObject("mniHome.Image")));
            this.mniHome.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.mniHome.Name = "mniHome";
            this.mniHome.Size = new System.Drawing.Size(55, 54);
            this.mniHome.Click += new System.EventHandler(this.mniHome_Click);
            // 
            // mniMaint
            // 
            this.mniMaint.BackColor = System.Drawing.Color.PowderBlue;
            this.mniMaint.Image = ((System.Drawing.Image)(resources.GetObject("mniMaint.Image")));
            this.mniMaint.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.mniMaint.Name = "mniMaint";
            this.mniMaint.Size = new System.Drawing.Size(55, 54);
            this.mniMaint.Click += new System.EventHandler(this.mniMaint_Click);
            // 
            // rentalsToolStripMenuItem
            // 
            this.rentalsToolStripMenuItem.BackColor = System.Drawing.Color.PowderBlue;
            this.rentalsToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("rentalsToolStripMenuItem.Image")));
            this.rentalsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.rentalsToolStripMenuItem.Name = "rentalsToolStripMenuItem";
            this.rentalsToolStripMenuItem.Size = new System.Drawing.Size(55, 54);
            this.rentalsToolStripMenuItem.Click += new System.EventHandler(this.rentalsToolStripMenuItem_Click);
            // 
            // tsiCalendar
            // 
            this.tsiCalendar.Image = global::Smart.Properties.Resources.calendar_icon1;
            this.tsiCalendar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsiCalendar.Name = "tsiCalendar";
            this.tsiCalendar.Size = new System.Drawing.Size(55, 54);
            this.tsiCalendar.Click += new System.EventHandler(this.tsiCalendar_Click);
            // 
            // stockToolStripMenuItem
            // 
            this.stockToolStripMenuItem.BackColor = System.Drawing.Color.PowderBlue;
            this.stockToolStripMenuItem.Image = global::Smart.Properties.Resources.tyres;
            this.stockToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.stockToolStripMenuItem.Name = "stockToolStripMenuItem";
            this.stockToolStripMenuItem.Size = new System.Drawing.Size(55, 54);
            this.stockToolStripMenuItem.Click += new System.EventHandler(this.stockToolStripMenuItem_Click);
            // 
            // poundToolStripMenuItem
            // 
            this.poundToolStripMenuItem.Image = global::Smart.Properties.Resources.Pound;
            this.poundToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.poundToolStripMenuItem.Name = "poundToolStripMenuItem";
            this.poundToolStripMenuItem.Size = new System.Drawing.Size(55, 54);
            // 
            // reoprtsToolStripMenuItem
            // 
            this.reoprtsToolStripMenuItem.BackColor = System.Drawing.Color.PowderBlue;
            this.reoprtsToolStripMenuItem.Image = global::Smart.Properties.Resources.pie_chart;
            this.reoprtsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.reoprtsToolStripMenuItem.Name = "reoprtsToolStripMenuItem";
            this.reoprtsToolStripMenuItem.Size = new System.Drawing.Size(55, 54);
            // 
            // adminToolStripMenuItem
            // 
            this.adminToolStripMenuItem.Image = global::Smart.Properties.Resources.three_users_icon_hi;
            this.adminToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.adminToolStripMenuItem.Name = "adminToolStripMenuItem";
            this.adminToolStripMenuItem.Size = new System.Drawing.Size(55, 54);
            this.adminToolStripMenuItem.Click += new System.EventHandler(this.adminToolStripMenuItem_Click);
            // 
            // calendarToolStripMenuItem
            // 
            this.calendarToolStripMenuItem.Name = "calendarToolStripMenuItem";
            this.calendarToolStripMenuItem.Size = new System.Drawing.Size(55, 4);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(1048, 526);
            this.Controls.Add(this.menuMain);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMART";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem mniHome;
        private System.Windows.Forms.ToolStripMenuItem mniMaint;
        private System.Windows.Forms.ToolStripMenuItem rentalsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calendarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem poundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reoprtsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem adminToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsiCalendar;
    }
}

