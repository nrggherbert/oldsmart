﻿namespace Smart
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.serviceRequestHeaderIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.initialRequestDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.initialRequestTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deployedTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deployedServiceLocTOADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deployedServiceLocTOCDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.deployedServiceLocETADataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.signOffSignedOffDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.initialLocationCategoryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eTADescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eTAMinutesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.requestCancelledDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.initialRequestTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.operatorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.summaryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uspSMARTactiveBreakdownsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ramsdevDataSet = new Smart.ramsdevDataSet();
            this.label2 = new System.Windows.Forms.Label();
            this.ramsDev1 = new Smart.ramsDev();
            this.usp_SMART_activeBreakdownsTableAdapter = new Smart.ramsdevDataSetTableAdapters.usp_SMART_activeBreakdownsTableAdapter();
            this.dsVehicleBudget = new Smart.dsVehicleBudget();
            this.dsVehicleBudgetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dsVehicleBudgetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.dashboardViewer1 = new DevExpress.DashboardWin.DashboardViewer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTactiveBreakdownsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsDev1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicleBudget)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicleBudgetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicleBudgetBindingSource1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dashboardViewer1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblWelcome);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(918, 69);
            this.panel1.TabIndex = 0;
            // 
            // lblWelcome
            // 
            this.lblWelcome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWelcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.Location = new System.Drawing.Point(0, 0);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(918, 69);
            this.lblWelcome.TabIndex = 0;
            this.lblWelcome.Text = "Welcome <User>.  Here are your messages for today";
            this.lblWelcome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 295);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(918, 312);
            this.panel2.TabIndex = 2;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.serviceRequestHeaderIDDataGridViewTextBoxColumn,
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn,
            this.customerGroupNameDataGridViewTextBoxColumn,
            this.initialRequestDateDataGridViewTextBoxColumn,
            this.initialRequestTimeDataGridViewTextBoxColumn,
            this.deployedTimeDataGridViewTextBoxColumn,
            this.deployedServiceLocTOADataGridViewTextBoxColumn,
            this.deployedServiceLocTOCDataGridViewTextBoxColumn,
            this.deployedServiceLocETADataGridViewTextBoxColumn,
            this.signOffSignedOffDataGridViewCheckBoxColumn,
            this.initialLocationCategoryDataGridViewTextBoxColumn,
            this.eTADescriptionDataGridViewTextBoxColumn,
            this.eTAMinutesDataGridViewTextBoxColumn,
            this.requestCancelledDataGridViewCheckBoxColumn,
            this.initialRequestTypeDataGridViewTextBoxColumn,
            this.operatorDataGridViewTextBoxColumn,
            this.summaryDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.uspSMARTactiveBreakdownsBindingSource;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 40);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(918, 272);
            this.dataGridView1.TabIndex = 1;
            // 
            // serviceRequestHeaderIDDataGridViewTextBoxColumn
            // 
            this.serviceRequestHeaderIDDataGridViewTextBoxColumn.DataPropertyName = "ServiceRequest_Header_ID";
            this.serviceRequestHeaderIDDataGridViewTextBoxColumn.HeaderText = "ServiceRequest_Header_ID";
            this.serviceRequestHeaderIDDataGridViewTextBoxColumn.Name = "serviceRequestHeaderIDDataGridViewTextBoxColumn";
            this.serviceRequestHeaderIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // customerFleetlistVehicleRegDataGridViewTextBoxColumn
            // 
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn.DataPropertyName = "Customer_Fleetlist_VehicleReg";
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn.HeaderText = "Customer_Fleetlist_VehicleReg";
            this.customerFleetlistVehicleRegDataGridViewTextBoxColumn.Name = "customerFleetlistVehicleRegDataGridViewTextBoxColumn";
            // 
            // customerGroupNameDataGridViewTextBoxColumn
            // 
            this.customerGroupNameDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Name";
            this.customerGroupNameDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Name";
            this.customerGroupNameDataGridViewTextBoxColumn.Name = "customerGroupNameDataGridViewTextBoxColumn";
            this.customerGroupNameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // initialRequestDateDataGridViewTextBoxColumn
            // 
            this.initialRequestDateDataGridViewTextBoxColumn.DataPropertyName = "Initial_Request_Date";
            this.initialRequestDateDataGridViewTextBoxColumn.HeaderText = "Initial_Request_Date";
            this.initialRequestDateDataGridViewTextBoxColumn.Name = "initialRequestDateDataGridViewTextBoxColumn";
            // 
            // initialRequestTimeDataGridViewTextBoxColumn
            // 
            this.initialRequestTimeDataGridViewTextBoxColumn.DataPropertyName = "Initial_Request_Time";
            this.initialRequestTimeDataGridViewTextBoxColumn.HeaderText = "Initial_Request_Time";
            this.initialRequestTimeDataGridViewTextBoxColumn.Name = "initialRequestTimeDataGridViewTextBoxColumn";
            // 
            // deployedTimeDataGridViewTextBoxColumn
            // 
            this.deployedTimeDataGridViewTextBoxColumn.DataPropertyName = "Deployed_Time";
            this.deployedTimeDataGridViewTextBoxColumn.HeaderText = "Deployed_Time";
            this.deployedTimeDataGridViewTextBoxColumn.Name = "deployedTimeDataGridViewTextBoxColumn";
            // 
            // deployedServiceLocTOADataGridViewTextBoxColumn
            // 
            this.deployedServiceLocTOADataGridViewTextBoxColumn.DataPropertyName = "Deployed_Service_Loc_TOA";
            this.deployedServiceLocTOADataGridViewTextBoxColumn.HeaderText = "Deployed_Service_Loc_TOA";
            this.deployedServiceLocTOADataGridViewTextBoxColumn.Name = "deployedServiceLocTOADataGridViewTextBoxColumn";
            // 
            // deployedServiceLocTOCDataGridViewTextBoxColumn
            // 
            this.deployedServiceLocTOCDataGridViewTextBoxColumn.DataPropertyName = "Deployed_Service_Loc_TOC";
            this.deployedServiceLocTOCDataGridViewTextBoxColumn.HeaderText = "Deployed_Service_Loc_TOC";
            this.deployedServiceLocTOCDataGridViewTextBoxColumn.Name = "deployedServiceLocTOCDataGridViewTextBoxColumn";
            // 
            // deployedServiceLocETADataGridViewTextBoxColumn
            // 
            this.deployedServiceLocETADataGridViewTextBoxColumn.DataPropertyName = "Deployed_Service_Loc_ETA";
            this.deployedServiceLocETADataGridViewTextBoxColumn.HeaderText = "Deployed_Service_Loc_ETA";
            this.deployedServiceLocETADataGridViewTextBoxColumn.Name = "deployedServiceLocETADataGridViewTextBoxColumn";
            // 
            // signOffSignedOffDataGridViewCheckBoxColumn
            // 
            this.signOffSignedOffDataGridViewCheckBoxColumn.DataPropertyName = "SignOff_SignedOff";
            this.signOffSignedOffDataGridViewCheckBoxColumn.HeaderText = "SignOff_SignedOff";
            this.signOffSignedOffDataGridViewCheckBoxColumn.Name = "signOffSignedOffDataGridViewCheckBoxColumn";
            // 
            // initialLocationCategoryDataGridViewTextBoxColumn
            // 
            this.initialLocationCategoryDataGridViewTextBoxColumn.DataPropertyName = "Initial_Location_Category";
            this.initialLocationCategoryDataGridViewTextBoxColumn.HeaderText = "Initial_Location_Category";
            this.initialLocationCategoryDataGridViewTextBoxColumn.Name = "initialLocationCategoryDataGridViewTextBoxColumn";
            // 
            // eTADescriptionDataGridViewTextBoxColumn
            // 
            this.eTADescriptionDataGridViewTextBoxColumn.DataPropertyName = "ETA_Description";
            this.eTADescriptionDataGridViewTextBoxColumn.HeaderText = "ETA_Description";
            this.eTADescriptionDataGridViewTextBoxColumn.Name = "eTADescriptionDataGridViewTextBoxColumn";
            // 
            // eTAMinutesDataGridViewTextBoxColumn
            // 
            this.eTAMinutesDataGridViewTextBoxColumn.DataPropertyName = "ETA_Minutes";
            this.eTAMinutesDataGridViewTextBoxColumn.HeaderText = "ETA_Minutes";
            this.eTAMinutesDataGridViewTextBoxColumn.Name = "eTAMinutesDataGridViewTextBoxColumn";
            // 
            // requestCancelledDataGridViewCheckBoxColumn
            // 
            this.requestCancelledDataGridViewCheckBoxColumn.DataPropertyName = "Request_Cancelled";
            this.requestCancelledDataGridViewCheckBoxColumn.HeaderText = "Request_Cancelled";
            this.requestCancelledDataGridViewCheckBoxColumn.Name = "requestCancelledDataGridViewCheckBoxColumn";
            // 
            // initialRequestTypeDataGridViewTextBoxColumn
            // 
            this.initialRequestTypeDataGridViewTextBoxColumn.DataPropertyName = "Initial_Request_Type";
            this.initialRequestTypeDataGridViewTextBoxColumn.HeaderText = "Initial_Request_Type";
            this.initialRequestTypeDataGridViewTextBoxColumn.Name = "initialRequestTypeDataGridViewTextBoxColumn";
            // 
            // operatorDataGridViewTextBoxColumn
            // 
            this.operatorDataGridViewTextBoxColumn.DataPropertyName = "Operator";
            this.operatorDataGridViewTextBoxColumn.HeaderText = "Operator";
            this.operatorDataGridViewTextBoxColumn.Name = "operatorDataGridViewTextBoxColumn";
            this.operatorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // summaryDataGridViewTextBoxColumn
            // 
            this.summaryDataGridViewTextBoxColumn.DataPropertyName = "Summary";
            this.summaryDataGridViewTextBoxColumn.HeaderText = "Summary";
            this.summaryDataGridViewTextBoxColumn.Name = "summaryDataGridViewTextBoxColumn";
            this.summaryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // uspSMARTactiveBreakdownsBindingSource
            // 
            this.uspSMARTactiveBreakdownsBindingSource.DataMember = "usp_SMART_activeBreakdowns";
            this.uspSMARTactiveBreakdownsBindingSource.DataSource = this.ramsdevDataSet;
            // 
            // ramsdevDataSet
            // 
            this.ramsdevDataSet.DataSetName = "ramsdevDataSet";
            this.ramsdevDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Red;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(918, 40);
            this.label2.TabIndex = 0;
            this.label2.Text = "Active Breakdowns";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ramsDev1
            // 
            this.ramsDev1.DataSetName = "ramsDev";
            this.ramsDev1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usp_SMART_activeBreakdownsTableAdapter
            // 
            this.usp_SMART_activeBreakdownsTableAdapter.ClearBeforeFill = true;
            // 
            // dsVehicleBudget
            // 
            this.dsVehicleBudget.DataSetName = "dsVehicleBudget";
            this.dsVehicleBudget.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dsVehicleBudgetBindingSource
            // 
            this.dsVehicleBudgetBindingSource.DataSource = this.dsVehicleBudget;
            this.dsVehicleBudgetBindingSource.Position = 0;
            // 
            // dsVehicleBudgetBindingSource1
            // 
            this.dsVehicleBudgetBindingSource1.DataSource = this.dsVehicleBudget;
            this.dsVehicleBudgetBindingSource1.Position = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.dashboardViewer1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 69);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(918, 226);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // dashboardViewer1
            // 
            this.dashboardViewer1.CustomDBSchemaProviderEx = null;
            this.dashboardViewer1.DashboardSource = typeof(Smart.dashMain);
            this.dashboardViewer1.Location = new System.Drawing.Point(3, 3);
            this.dashboardViewer1.Name = "dashboardViewer1";
            this.dashboardViewer1.Size = new System.Drawing.Size(915, 230);
            this.dashboardViewer1.TabIndex = 0;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(918, 477);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMain";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTactiveBreakdownsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramsDev1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicleBudget)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicleBudgetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicleBudgetBindingSource1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dashboardViewer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Panel panel2;
        private ramsDev ramsDev1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private ramsdevDataSet ramsdevDataSet;
        private System.Windows.Forms.BindingSource uspSMARTactiveBreakdownsBindingSource;
        private ramsdevDataSetTableAdapters.usp_SMART_activeBreakdownsTableAdapter usp_SMART_activeBreakdownsTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceRequestHeaderIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerFleetlistVehicleRegDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn initialRequestDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn initialRequestTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deployedTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deployedServiceLocTOADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deployedServiceLocTOCDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn deployedServiceLocETADataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn signOffSignedOffDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn initialLocationCategoryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eTADescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eTAMinutesDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn requestCancelledDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn initialRequestTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn operatorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn summaryDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource dsVehicleBudgetBindingSource;
        private dsVehicleBudget dsVehicleBudget;
        private System.Windows.Forms.BindingSource dsVehicleBudgetBindingSource1;
        private DevExpress.DashboardWin.DashboardViewer dashboardViewer1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
    }
}