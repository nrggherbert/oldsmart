﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Smart
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            MenuStrip ms = new MenuStrip();
            ToolStripMenuItem windowMenu = new ToolStripMenuItem("Window");
            ToolStripMenuItem windowNewMenu = new ToolStripMenuItem("New");
            windowMenu.DropDownItems.Add(windowNewMenu);
            ((ToolStripDropDownMenu)(windowMenu.DropDown)).ShowImageMargin = false;
            ((ToolStripDropDownMenu)(windowMenu.DropDown)).ShowCheckMargin = true;

            // Assign the ToolStripMenuItem that displays 
            // the list of child forms.
            ms.MdiWindowListItem = windowMenu;

            // Add the window ToolStripMenuItem to the MenuStrip.
            ms.Items.Add(windowMenu);

            // Dock the MenuStrip to the top of the form.
            ms.Dock = DockStyle.Top;

            // The Form.MainMenuStrip property determines the merge target.
            this.MainMenuStrip = ms;

            this.Controls.Add(ms);

        }

        private void closeCurrent()
        {
            /*
            if (this.ActiveMdiChild != null)
            {
                this.ActiveMdiChild.Close();
            }
            */

        }


        private void resetMenus()
        {
            foreach (ToolStripMenuItem mi in menuMain.Items)
            {
                mi.BackColor = System.Drawing.Color.PowderBlue;
            }

        }

        private void mniHome_Click(object sender, EventArgs e)
        {
            resetMenus();
            mniHome.BackColor = System.Drawing.Color.Orange;

            closeCurrent();
            //this.MdiChildren. .Contains(frmMain)

            

            frmMain newMDIChild = new frmMain()
            {
                MdiParent = this,
                //Dock = DockStyle.Fill
            };
            newMDIChild.Show();
        }

        private void mniMaint_Click(object sender, EventArgs e)
        {
            resetMenus();
            mniMaint.BackColor = System.Drawing.Color.Orange;
            closeCurrent();
            frmMaint newMDIChild = new frmMaint()
            {
                MdiParent = this,
                //Dock = DockStyle.Fill
            };
            newMDIChild.Show();

        }

        private void rentalsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetMenus();
            rentalsToolStripMenuItem.BackColor = System.Drawing.Color.Orange;
            closeCurrent();
            frmRentals newMDIChild = new frmRentals()
            {
                MdiParent = this,
                //Dock = DockStyle.Fill
            };
            newMDIChild.Show();

        }

        private void stockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetMenus();
            stockToolStripMenuItem.BackColor = System.Drawing.Color.Orange;
            closeCurrent();
            frmStock newMDIChild = new frmStock()
            {
                MdiParent = this,
                //Dock = DockStyle.Fill
            };
            newMDIChild.Show();

        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("cName", typeof(string));
            dt.Columns.Add("left", typeof(int));
            dt.Columns.Add("top", typeof(int));
            ds.Tables.Add(dt);
            foreach (Control c in this.Controls)
            {
                dt.Rows.Add(c.Name, c.Left, c.Top);

            }
            
            ds.WriteXml("c:\\smart\\smartpos.xml");
            */

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*
            if (File.Exists("c:\\smart\\smartpos.xml"))
                {
                DataSet ds = new DataSet();
                ds.ReadXml("c:\\smart\\smartpos.xml");
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {

                }
            }
            */
        }

        private void adminToolStripMenuItem_Click(object sender, EventArgs e)
        {
            resetMenus();
            rentalsToolStripMenuItem.BackColor = System.Drawing.Color.Orange;
            closeCurrent();
            frmAdmin newMDIChild = new frmAdmin()
            {
                MdiParent = this,
                Dock = DockStyle.Fill
            };
            newMDIChild.Show();

        }

        private void tsiCalendar_Click(object sender, EventArgs e)
        {
            resetMenus();
            rentalsToolStripMenuItem.BackColor = System.Drawing.Color.Orange;
            closeCurrent();
            frmCompliance newMDIChild = new frmCompliance()
            {
                MdiParent = this,
                //Dock = DockStyle.Fill
            };
            newMDIChild.Show();

        }
    }
}
