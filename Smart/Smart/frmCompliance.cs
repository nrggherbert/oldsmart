﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Calendar.NET;
using DevExpress.XtraScheduler;

namespace Smart
{
    public partial class frmCompliance : Form
    {
        public frmCompliance()
        {
            InitializeComponent();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Name == "tpSchedule")
            {
                calendar1.CalendarView = CalendarViews.Month;
                calendar1.AllowEditingEvents = true;

                var MOT = new CustomEvent
                {
                    Date = DateTime.Now,
                    RecurringFrequency = RecurringFrequencies.Yearly,
                    EventText = "MOT"
                };
                calendar1.AddEvent(MOT);


                DateTime dt = new DateTime(2017, 8, 5);
                var Service = new CustomEvent
                {
                    Date = dt,
                    RecurringFrequency = RecurringFrequencies.Monthly,
                    EventText = "A Service"
                };
                calendar1.AddEvent(Service);
            }
        }

        private void calendarControl1_Click(object sender, EventArgs e)
        {

        }

        private void frmCompliance_Load(object sender, EventArgs e)
        {

        }
    }
}
