﻿namespace Smart
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.uspSMARTgetCustomerLocationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.smartdevDataSet6 = new Smart.smartdevDataSet6();
            this.uspSMARTgetCustomerGroupsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.smartdevDataSet5 = new Smart.smartdevDataSet5();
            this.usp_SMART_getCustomerGroupsTableAdapter = new Smart.smartdevDataSet5TableAdapters.usp_SMART_getCustomerGroupsTableAdapter();
            this.usp_SMART_getCustomerLocationsTableAdapter = new Smart.smartdevDataSet6TableAdapters.usp_SMART_getCustomerLocationsTableAdapter();
            this.tpCustomers = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbExport = new System.Windows.Forms.ProgressBar();
            this.dgvLocs = new System.Windows.Forms.DataGridView();
            this.customerGroupIDDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocAddress1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocAddress2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocAddress3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocAddress4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocCountyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocPostcode1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocPostcode2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocCountryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocPrimaryContactDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocSecondaryContactDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocTelephoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocFaxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocEmailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceProviderLocIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLocActiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customerInvNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerInvAddress1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerInvAddress2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerInvAddress3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerInvAddress4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerInvCountyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerInvPostcode1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerInvPostcode2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerInvCountryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastVisitDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastVisitByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.casingsEnterCasingCollectionChainDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.forArchiveDataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.alertIllegalRemovalDepthEmailToDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.notificationsEmailDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.notificationsTextDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dgvGroups = new System.Windows.Forms.DataGridView();
            this.customerSuperGroupIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupAddress1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupAddress2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupAddress3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupAddress4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupCountyDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupPostcode1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupPostcode2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupCountryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupPrimaryNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupPrimaryTelephoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupPrimaryFaxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupPrimaryEmailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupSecondaryNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupSecondaryTelephoneDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupSecondaryFaxDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupSecondaryEmailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupActiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customerGroupAccountNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupInvoiceOnlyAccountDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customerGroupIncludeForSalesVATDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customerGroupZeroRatedVatDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customerGroupAllowZeroValueItemsDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customerGroupOrderNumberRequiredDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.policySteer1stDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.policySteer2ndDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.policyDrive1stDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.policyDrive2ndDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.policyTrailer1stDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.policyTrailer2ndDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rdPartyCustomerNamePromptDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customerGroupPricingRenewalDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupDaysBetweenInspectionsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupPaymentDaysDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerGroupOrderNumberRequestedAtICDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.salesmanInitialsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.useGLPBuyingPricesDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.showToServiceProvidersDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.isBankFinancedDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.majorRepairDestDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forDeteDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.customerGroupCreditLimitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forArchiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.retorqueTextDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.retorqueReminderActiveDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.affilliateLogoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.managedFleetDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cmsGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miExport = new System.Windows.Forms.ToolStripMenuItem();
            this.tpUsers = new System.Windows.Forms.TabPage();
            this.tcAdmin = new System.Windows.Forms.TabControl();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCustomerLocationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCustomerGroupsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet5)).BeginInit();
            this.tpCustomers.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroups)).BeginInit();
            this.cmsGrid.SuspendLayout();
            this.tcAdmin.SuspendLayout();
            this.SuspendLayout();
            // 
            // uspSMARTgetCustomerLocationsBindingSource
            // 
            this.uspSMARTgetCustomerLocationsBindingSource.DataMember = "usp_SMART_getCustomerLocations";
            this.uspSMARTgetCustomerLocationsBindingSource.DataSource = this.smartdevDataSet6;
            // 
            // smartdevDataSet6
            // 
            this.smartdevDataSet6.DataSetName = "smartdevDataSet6";
            this.smartdevDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uspSMARTgetCustomerGroupsBindingSource
            // 
            this.uspSMARTgetCustomerGroupsBindingSource.DataMember = "usp_SMART_getCustomerGroups";
            this.uspSMARTgetCustomerGroupsBindingSource.DataSource = this.smartdevDataSet5;
            // 
            // smartdevDataSet5
            // 
            this.smartdevDataSet5.DataSetName = "smartdevDataSet5";
            this.smartdevDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usp_SMART_getCustomerGroupsTableAdapter
            // 
            this.usp_SMART_getCustomerGroupsTableAdapter.ClearBeforeFill = true;
            // 
            // usp_SMART_getCustomerLocationsTableAdapter
            // 
            this.usp_SMART_getCustomerLocationsTableAdapter.ClearBeforeFill = true;
            // 
            // tpCustomers
            // 
            this.tpCustomers.Controls.Add(this.panel1);
            this.tpCustomers.Controls.Add(this.dgvLocs);
            this.tpCustomers.Controls.Add(this.dgvGroups);
            this.tpCustomers.Location = new System.Drawing.Point(4, 29);
            this.tpCustomers.Name = "tpCustomers";
            this.tpCustomers.Padding = new System.Windows.Forms.Padding(3);
            this.tpCustomers.Size = new System.Drawing.Size(1312, 685);
            this.tpCustomers.TabIndex = 1;
            this.tpCustomers.Text = "Customers";
            this.tpCustomers.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pbExport);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 582);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1306, 100);
            this.panel1.TabIndex = 3;
            // 
            // pbExport
            // 
            this.pbExport.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.pbExport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbExport.Location = new System.Drawing.Point(0, 0);
            this.pbExport.Name = "pbExport";
            this.pbExport.Size = new System.Drawing.Size(1306, 100);
            this.pbExport.TabIndex = 3;
            this.pbExport.Visible = false;
            // 
            // dgvLocs
            // 
            this.dgvLocs.AllowUserToAddRows = false;
            this.dgvLocs.AllowUserToDeleteRows = false;
            this.dgvLocs.AllowUserToOrderColumns = true;
            this.dgvLocs.AutoGenerateColumns = false;
            this.dgvLocs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLocs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.customerGroupIDDataGridViewTextBoxColumn1,
            this.customerLocIDDataGridViewTextBoxColumn,
            this.customerLocNameDataGridViewTextBoxColumn,
            this.customerLocAddress1DataGridViewTextBoxColumn,
            this.customerLocAddress2DataGridViewTextBoxColumn,
            this.customerLocAddress3DataGridViewTextBoxColumn,
            this.customerLocAddress4DataGridViewTextBoxColumn,
            this.customerLocCountyDataGridViewTextBoxColumn,
            this.customerLocPostcode1DataGridViewTextBoxColumn,
            this.customerLocPostcode2DataGridViewTextBoxColumn,
            this.customerLocCountryDataGridViewTextBoxColumn,
            this.customerLocPrimaryContactDataGridViewTextBoxColumn,
            this.customerLocSecondaryContactDataGridViewTextBoxColumn,
            this.customerLocTelephoneDataGridViewTextBoxColumn,
            this.customerLocFaxDataGridViewTextBoxColumn,
            this.customerLocEmailDataGridViewTextBoxColumn,
            this.serviceProviderLocIDDataGridViewTextBoxColumn,
            this.customerLocActiveDataGridViewCheckBoxColumn,
            this.customerInvNameDataGridViewTextBoxColumn,
            this.customerInvAddress1DataGridViewTextBoxColumn,
            this.customerInvAddress2DataGridViewTextBoxColumn,
            this.customerInvAddress3DataGridViewTextBoxColumn,
            this.customerInvAddress4DataGridViewTextBoxColumn,
            this.customerInvCountyDataGridViewTextBoxColumn,
            this.customerInvPostcode1DataGridViewTextBoxColumn,
            this.customerInvPostcode2DataGridViewTextBoxColumn,
            this.customerInvCountryDataGridViewTextBoxColumn,
            this.lastVisitDateDataGridViewTextBoxColumn,
            this.lastVisitByDataGridViewTextBoxColumn,
            this.casingsEnterCasingCollectionChainDataGridViewCheckBoxColumn,
            this.forArchiveDataGridViewCheckBoxColumn1,
            this.alertIllegalRemovalDepthEmailToDataGridViewTextBoxColumn,
            this.notificationsEmailDataGridViewCheckBoxColumn,
            this.notificationsTextDataGridViewCheckBoxColumn});
            this.dgvLocs.DataSource = this.uspSMARTgetCustomerLocationsBindingSource;
            this.dgvLocs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvLocs.Location = new System.Drawing.Point(3, 440);
            this.dgvLocs.Name = "dgvLocs";
            this.dgvLocs.Size = new System.Drawing.Size(1306, 242);
            this.dgvLocs.TabIndex = 1;
            // 
            // customerGroupIDDataGridViewTextBoxColumn1
            // 
            this.customerGroupIDDataGridViewTextBoxColumn1.DataPropertyName = "Customer_GroupID";
            this.customerGroupIDDataGridViewTextBoxColumn1.HeaderText = "Customer_GroupID";
            this.customerGroupIDDataGridViewTextBoxColumn1.Name = "customerGroupIDDataGridViewTextBoxColumn1";
            // 
            // customerLocIDDataGridViewTextBoxColumn
            // 
            this.customerLocIDDataGridViewTextBoxColumn.DataPropertyName = "Customer_LocID";
            this.customerLocIDDataGridViewTextBoxColumn.HeaderText = "Customer_LocID";
            this.customerLocIDDataGridViewTextBoxColumn.Name = "customerLocIDDataGridViewTextBoxColumn";
            this.customerLocIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // customerLocNameDataGridViewTextBoxColumn
            // 
            this.customerLocNameDataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Name";
            this.customerLocNameDataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Name";
            this.customerLocNameDataGridViewTextBoxColumn.Name = "customerLocNameDataGridViewTextBoxColumn";
            // 
            // customerLocAddress1DataGridViewTextBoxColumn
            // 
            this.customerLocAddress1DataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Address1";
            this.customerLocAddress1DataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Address1";
            this.customerLocAddress1DataGridViewTextBoxColumn.Name = "customerLocAddress1DataGridViewTextBoxColumn";
            // 
            // customerLocAddress2DataGridViewTextBoxColumn
            // 
            this.customerLocAddress2DataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Address2";
            this.customerLocAddress2DataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Address2";
            this.customerLocAddress2DataGridViewTextBoxColumn.Name = "customerLocAddress2DataGridViewTextBoxColumn";
            // 
            // customerLocAddress3DataGridViewTextBoxColumn
            // 
            this.customerLocAddress3DataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Address3";
            this.customerLocAddress3DataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Address3";
            this.customerLocAddress3DataGridViewTextBoxColumn.Name = "customerLocAddress3DataGridViewTextBoxColumn";
            // 
            // customerLocAddress4DataGridViewTextBoxColumn
            // 
            this.customerLocAddress4DataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Address4";
            this.customerLocAddress4DataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Address4";
            this.customerLocAddress4DataGridViewTextBoxColumn.Name = "customerLocAddress4DataGridViewTextBoxColumn";
            // 
            // customerLocCountyDataGridViewTextBoxColumn
            // 
            this.customerLocCountyDataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_County";
            this.customerLocCountyDataGridViewTextBoxColumn.HeaderText = "Customer_Loc_County";
            this.customerLocCountyDataGridViewTextBoxColumn.Name = "customerLocCountyDataGridViewTextBoxColumn";
            // 
            // customerLocPostcode1DataGridViewTextBoxColumn
            // 
            this.customerLocPostcode1DataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Postcode1";
            this.customerLocPostcode1DataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Postcode1";
            this.customerLocPostcode1DataGridViewTextBoxColumn.Name = "customerLocPostcode1DataGridViewTextBoxColumn";
            // 
            // customerLocPostcode2DataGridViewTextBoxColumn
            // 
            this.customerLocPostcode2DataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Postcode2";
            this.customerLocPostcode2DataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Postcode2";
            this.customerLocPostcode2DataGridViewTextBoxColumn.Name = "customerLocPostcode2DataGridViewTextBoxColumn";
            // 
            // customerLocCountryDataGridViewTextBoxColumn
            // 
            this.customerLocCountryDataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Country";
            this.customerLocCountryDataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Country";
            this.customerLocCountryDataGridViewTextBoxColumn.Name = "customerLocCountryDataGridViewTextBoxColumn";
            // 
            // customerLocPrimaryContactDataGridViewTextBoxColumn
            // 
            this.customerLocPrimaryContactDataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Primary_Contact";
            this.customerLocPrimaryContactDataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Primary_Contact";
            this.customerLocPrimaryContactDataGridViewTextBoxColumn.Name = "customerLocPrimaryContactDataGridViewTextBoxColumn";
            // 
            // customerLocSecondaryContactDataGridViewTextBoxColumn
            // 
            this.customerLocSecondaryContactDataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Secondary_Contact";
            this.customerLocSecondaryContactDataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Secondary_Contact";
            this.customerLocSecondaryContactDataGridViewTextBoxColumn.Name = "customerLocSecondaryContactDataGridViewTextBoxColumn";
            // 
            // customerLocTelephoneDataGridViewTextBoxColumn
            // 
            this.customerLocTelephoneDataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Telephone";
            this.customerLocTelephoneDataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Telephone";
            this.customerLocTelephoneDataGridViewTextBoxColumn.Name = "customerLocTelephoneDataGridViewTextBoxColumn";
            // 
            // customerLocFaxDataGridViewTextBoxColumn
            // 
            this.customerLocFaxDataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Fax";
            this.customerLocFaxDataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Fax";
            this.customerLocFaxDataGridViewTextBoxColumn.Name = "customerLocFaxDataGridViewTextBoxColumn";
            // 
            // customerLocEmailDataGridViewTextBoxColumn
            // 
            this.customerLocEmailDataGridViewTextBoxColumn.DataPropertyName = "Customer_Loc_Email";
            this.customerLocEmailDataGridViewTextBoxColumn.HeaderText = "Customer_Loc_Email";
            this.customerLocEmailDataGridViewTextBoxColumn.Name = "customerLocEmailDataGridViewTextBoxColumn";
            // 
            // serviceProviderLocIDDataGridViewTextBoxColumn
            // 
            this.serviceProviderLocIDDataGridViewTextBoxColumn.DataPropertyName = "Service_Provider_LocID";
            this.serviceProviderLocIDDataGridViewTextBoxColumn.HeaderText = "Service_Provider_LocID";
            this.serviceProviderLocIDDataGridViewTextBoxColumn.Name = "serviceProviderLocIDDataGridViewTextBoxColumn";
            // 
            // customerLocActiveDataGridViewCheckBoxColumn
            // 
            this.customerLocActiveDataGridViewCheckBoxColumn.DataPropertyName = "Customer_Loc_Active";
            this.customerLocActiveDataGridViewCheckBoxColumn.HeaderText = "Customer_Loc_Active";
            this.customerLocActiveDataGridViewCheckBoxColumn.Name = "customerLocActiveDataGridViewCheckBoxColumn";
            // 
            // customerInvNameDataGridViewTextBoxColumn
            // 
            this.customerInvNameDataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_Name";
            this.customerInvNameDataGridViewTextBoxColumn.HeaderText = "Customer_Inv_Name";
            this.customerInvNameDataGridViewTextBoxColumn.Name = "customerInvNameDataGridViewTextBoxColumn";
            // 
            // customerInvAddress1DataGridViewTextBoxColumn
            // 
            this.customerInvAddress1DataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_Address1";
            this.customerInvAddress1DataGridViewTextBoxColumn.HeaderText = "Customer_Inv_Address1";
            this.customerInvAddress1DataGridViewTextBoxColumn.Name = "customerInvAddress1DataGridViewTextBoxColumn";
            // 
            // customerInvAddress2DataGridViewTextBoxColumn
            // 
            this.customerInvAddress2DataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_Address2";
            this.customerInvAddress2DataGridViewTextBoxColumn.HeaderText = "Customer_Inv_Address2";
            this.customerInvAddress2DataGridViewTextBoxColumn.Name = "customerInvAddress2DataGridViewTextBoxColumn";
            // 
            // customerInvAddress3DataGridViewTextBoxColumn
            // 
            this.customerInvAddress3DataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_Address3";
            this.customerInvAddress3DataGridViewTextBoxColumn.HeaderText = "Customer_Inv_Address3";
            this.customerInvAddress3DataGridViewTextBoxColumn.Name = "customerInvAddress3DataGridViewTextBoxColumn";
            // 
            // customerInvAddress4DataGridViewTextBoxColumn
            // 
            this.customerInvAddress4DataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_Address4";
            this.customerInvAddress4DataGridViewTextBoxColumn.HeaderText = "Customer_Inv_Address4";
            this.customerInvAddress4DataGridViewTextBoxColumn.Name = "customerInvAddress4DataGridViewTextBoxColumn";
            // 
            // customerInvCountyDataGridViewTextBoxColumn
            // 
            this.customerInvCountyDataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_County";
            this.customerInvCountyDataGridViewTextBoxColumn.HeaderText = "Customer_Inv_County";
            this.customerInvCountyDataGridViewTextBoxColumn.Name = "customerInvCountyDataGridViewTextBoxColumn";
            // 
            // customerInvPostcode1DataGridViewTextBoxColumn
            // 
            this.customerInvPostcode1DataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_Postcode1";
            this.customerInvPostcode1DataGridViewTextBoxColumn.HeaderText = "Customer_Inv_Postcode1";
            this.customerInvPostcode1DataGridViewTextBoxColumn.Name = "customerInvPostcode1DataGridViewTextBoxColumn";
            // 
            // customerInvPostcode2DataGridViewTextBoxColumn
            // 
            this.customerInvPostcode2DataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_Postcode2";
            this.customerInvPostcode2DataGridViewTextBoxColumn.HeaderText = "Customer_Inv_Postcode2";
            this.customerInvPostcode2DataGridViewTextBoxColumn.Name = "customerInvPostcode2DataGridViewTextBoxColumn";
            // 
            // customerInvCountryDataGridViewTextBoxColumn
            // 
            this.customerInvCountryDataGridViewTextBoxColumn.DataPropertyName = "Customer_Inv_Country";
            this.customerInvCountryDataGridViewTextBoxColumn.HeaderText = "Customer_Inv_Country";
            this.customerInvCountryDataGridViewTextBoxColumn.Name = "customerInvCountryDataGridViewTextBoxColumn";
            // 
            // lastVisitDateDataGridViewTextBoxColumn
            // 
            this.lastVisitDateDataGridViewTextBoxColumn.DataPropertyName = "Last_Visit_Date";
            this.lastVisitDateDataGridViewTextBoxColumn.HeaderText = "Last_Visit_Date";
            this.lastVisitDateDataGridViewTextBoxColumn.Name = "lastVisitDateDataGridViewTextBoxColumn";
            // 
            // lastVisitByDataGridViewTextBoxColumn
            // 
            this.lastVisitByDataGridViewTextBoxColumn.DataPropertyName = "Last_Visit_By";
            this.lastVisitByDataGridViewTextBoxColumn.HeaderText = "Last_Visit_By";
            this.lastVisitByDataGridViewTextBoxColumn.Name = "lastVisitByDataGridViewTextBoxColumn";
            // 
            // casingsEnterCasingCollectionChainDataGridViewCheckBoxColumn
            // 
            this.casingsEnterCasingCollectionChainDataGridViewCheckBoxColumn.DataPropertyName = "Casings_Enter_Casing_Collection_Chain";
            this.casingsEnterCasingCollectionChainDataGridViewCheckBoxColumn.HeaderText = "Casings_Enter_Casing_Collection_Chain";
            this.casingsEnterCasingCollectionChainDataGridViewCheckBoxColumn.Name = "casingsEnterCasingCollectionChainDataGridViewCheckBoxColumn";
            // 
            // forArchiveDataGridViewCheckBoxColumn1
            // 
            this.forArchiveDataGridViewCheckBoxColumn1.DataPropertyName = "For_Archive";
            this.forArchiveDataGridViewCheckBoxColumn1.HeaderText = "For_Archive";
            this.forArchiveDataGridViewCheckBoxColumn1.Name = "forArchiveDataGridViewCheckBoxColumn1";
            // 
            // alertIllegalRemovalDepthEmailToDataGridViewTextBoxColumn
            // 
            this.alertIllegalRemovalDepthEmailToDataGridViewTextBoxColumn.DataPropertyName = "Alert_Illegal_Removal_Depth_Email_To";
            this.alertIllegalRemovalDepthEmailToDataGridViewTextBoxColumn.HeaderText = "Alert_Illegal_Removal_Depth_Email_To";
            this.alertIllegalRemovalDepthEmailToDataGridViewTextBoxColumn.Name = "alertIllegalRemovalDepthEmailToDataGridViewTextBoxColumn";
            // 
            // notificationsEmailDataGridViewCheckBoxColumn
            // 
            this.notificationsEmailDataGridViewCheckBoxColumn.DataPropertyName = "Notifications_Email";
            this.notificationsEmailDataGridViewCheckBoxColumn.HeaderText = "Notifications_Email";
            this.notificationsEmailDataGridViewCheckBoxColumn.Name = "notificationsEmailDataGridViewCheckBoxColumn";
            // 
            // notificationsTextDataGridViewCheckBoxColumn
            // 
            this.notificationsTextDataGridViewCheckBoxColumn.DataPropertyName = "Notifications_Text";
            this.notificationsTextDataGridViewCheckBoxColumn.HeaderText = "Notifications_Text";
            this.notificationsTextDataGridViewCheckBoxColumn.Name = "notificationsTextDataGridViewCheckBoxColumn";
            // 
            // dgvGroups
            // 
            this.dgvGroups.AllowUserToAddRows = false;
            this.dgvGroups.AllowUserToDeleteRows = false;
            this.dgvGroups.AllowUserToOrderColumns = true;
            this.dgvGroups.AutoGenerateColumns = false;
            this.dgvGroups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvGroups.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.customerSuperGroupIDDataGridViewTextBoxColumn,
            this.customerGroupIDDataGridViewTextBoxColumn,
            this.customerGroupNameDataGridViewTextBoxColumn,
            this.customerGroupAddress1DataGridViewTextBoxColumn,
            this.customerGroupAddress2DataGridViewTextBoxColumn,
            this.customerGroupAddress3DataGridViewTextBoxColumn,
            this.customerGroupAddress4DataGridViewTextBoxColumn,
            this.customerGroupCountyDataGridViewTextBoxColumn,
            this.customerGroupPostcode1DataGridViewTextBoxColumn,
            this.customerGroupPostcode2DataGridViewTextBoxColumn,
            this.customerGroupCountryDataGridViewTextBoxColumn,
            this.customerGroupPrimaryNameDataGridViewTextBoxColumn,
            this.customerGroupPrimaryTelephoneDataGridViewTextBoxColumn,
            this.customerGroupPrimaryFaxDataGridViewTextBoxColumn,
            this.customerGroupPrimaryEmailDataGridViewTextBoxColumn,
            this.customerGroupSecondaryNameDataGridViewTextBoxColumn,
            this.customerGroupSecondaryTelephoneDataGridViewTextBoxColumn,
            this.customerGroupSecondaryFaxDataGridViewTextBoxColumn,
            this.customerGroupSecondaryEmailDataGridViewTextBoxColumn,
            this.customerGroupActiveDataGridViewCheckBoxColumn,
            this.customerGroupAccountNumberDataGridViewTextBoxColumn,
            this.customerGroupInvoiceOnlyAccountDataGridViewCheckBoxColumn,
            this.customerGroupIncludeForSalesVATDataGridViewCheckBoxColumn,
            this.customerGroupZeroRatedVatDataGridViewCheckBoxColumn,
            this.customerGroupAllowZeroValueItemsDataGridViewCheckBoxColumn,
            this.customerGroupOrderNumberRequiredDataGridViewCheckBoxColumn,
            this.policySteer1stDataGridViewTextBoxColumn,
            this.policySteer2ndDataGridViewTextBoxColumn,
            this.policyDrive1stDataGridViewTextBoxColumn,
            this.policyDrive2ndDataGridViewTextBoxColumn,
            this.policyTrailer1stDataGridViewTextBoxColumn,
            this.policyTrailer2ndDataGridViewTextBoxColumn,
            this.rdPartyCustomerNamePromptDataGridViewCheckBoxColumn,
            this.customerGroupPricingRenewalDateDataGridViewTextBoxColumn,
            this.customerGroupDaysBetweenInspectionsDataGridViewTextBoxColumn,
            this.customerGroupPaymentDaysDataGridViewTextBoxColumn,
            this.customerGroupOrderNumberRequestedAtICDataGridViewCheckBoxColumn,
            this.salesmanInitialsDataGridViewTextBoxColumn,
            this.useGLPBuyingPricesDataGridViewCheckBoxColumn,
            this.showToServiceProvidersDataGridViewCheckBoxColumn,
            this.isBankFinancedDataGridViewCheckBoxColumn,
            this.majorRepairDestDataGridViewTextBoxColumn,
            this.forDeteDataGridViewCheckBoxColumn,
            this.customerGroupCreditLimitDataGridViewTextBoxColumn,
            this.forArchiveDataGridViewCheckBoxColumn,
            this.retorqueTextDataGridViewTextBoxColumn,
            this.retorqueReminderActiveDataGridViewCheckBoxColumn,
            this.affilliateLogoDataGridViewTextBoxColumn,
            this.managedFleetDataGridViewCheckBoxColumn});
            this.dgvGroups.ContextMenuStrip = this.cmsGrid;
            this.dgvGroups.DataSource = this.uspSMARTgetCustomerGroupsBindingSource;
            this.dgvGroups.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvGroups.Location = new System.Drawing.Point(3, 3);
            this.dgvGroups.Name = "dgvGroups";
            this.dgvGroups.Size = new System.Drawing.Size(1306, 437);
            this.dgvGroups.TabIndex = 0;
            // 
            // customerSuperGroupIDDataGridViewTextBoxColumn
            // 
            this.customerSuperGroupIDDataGridViewTextBoxColumn.DataPropertyName = "Customer_SuperGroupID";
            this.customerSuperGroupIDDataGridViewTextBoxColumn.HeaderText = "Customer_SuperGroupID";
            this.customerSuperGroupIDDataGridViewTextBoxColumn.Name = "customerSuperGroupIDDataGridViewTextBoxColumn";
            // 
            // customerGroupIDDataGridViewTextBoxColumn
            // 
            this.customerGroupIDDataGridViewTextBoxColumn.DataPropertyName = "Customer_GroupID";
            this.customerGroupIDDataGridViewTextBoxColumn.HeaderText = "Customer_GroupID";
            this.customerGroupIDDataGridViewTextBoxColumn.Name = "customerGroupIDDataGridViewTextBoxColumn";
            this.customerGroupIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // customerGroupNameDataGridViewTextBoxColumn
            // 
            this.customerGroupNameDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Name";
            this.customerGroupNameDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Name";
            this.customerGroupNameDataGridViewTextBoxColumn.Name = "customerGroupNameDataGridViewTextBoxColumn";
            // 
            // customerGroupAddress1DataGridViewTextBoxColumn
            // 
            this.customerGroupAddress1DataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Address1";
            this.customerGroupAddress1DataGridViewTextBoxColumn.HeaderText = "Customer_Group_Address1";
            this.customerGroupAddress1DataGridViewTextBoxColumn.Name = "customerGroupAddress1DataGridViewTextBoxColumn";
            // 
            // customerGroupAddress2DataGridViewTextBoxColumn
            // 
            this.customerGroupAddress2DataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Address2";
            this.customerGroupAddress2DataGridViewTextBoxColumn.HeaderText = "Customer_Group_Address2";
            this.customerGroupAddress2DataGridViewTextBoxColumn.Name = "customerGroupAddress2DataGridViewTextBoxColumn";
            // 
            // customerGroupAddress3DataGridViewTextBoxColumn
            // 
            this.customerGroupAddress3DataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Address3";
            this.customerGroupAddress3DataGridViewTextBoxColumn.HeaderText = "Customer_Group_Address3";
            this.customerGroupAddress3DataGridViewTextBoxColumn.Name = "customerGroupAddress3DataGridViewTextBoxColumn";
            // 
            // customerGroupAddress4DataGridViewTextBoxColumn
            // 
            this.customerGroupAddress4DataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Address4";
            this.customerGroupAddress4DataGridViewTextBoxColumn.HeaderText = "Customer_Group_Address4";
            this.customerGroupAddress4DataGridViewTextBoxColumn.Name = "customerGroupAddress4DataGridViewTextBoxColumn";
            // 
            // customerGroupCountyDataGridViewTextBoxColumn
            // 
            this.customerGroupCountyDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_County";
            this.customerGroupCountyDataGridViewTextBoxColumn.HeaderText = "Customer_Group_County";
            this.customerGroupCountyDataGridViewTextBoxColumn.Name = "customerGroupCountyDataGridViewTextBoxColumn";
            // 
            // customerGroupPostcode1DataGridViewTextBoxColumn
            // 
            this.customerGroupPostcode1DataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Postcode1";
            this.customerGroupPostcode1DataGridViewTextBoxColumn.HeaderText = "Customer_Group_Postcode1";
            this.customerGroupPostcode1DataGridViewTextBoxColumn.Name = "customerGroupPostcode1DataGridViewTextBoxColumn";
            // 
            // customerGroupPostcode2DataGridViewTextBoxColumn
            // 
            this.customerGroupPostcode2DataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Postcode2";
            this.customerGroupPostcode2DataGridViewTextBoxColumn.HeaderText = "Customer_Group_Postcode2";
            this.customerGroupPostcode2DataGridViewTextBoxColumn.Name = "customerGroupPostcode2DataGridViewTextBoxColumn";
            // 
            // customerGroupCountryDataGridViewTextBoxColumn
            // 
            this.customerGroupCountryDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Country";
            this.customerGroupCountryDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Country";
            this.customerGroupCountryDataGridViewTextBoxColumn.Name = "customerGroupCountryDataGridViewTextBoxColumn";
            // 
            // customerGroupPrimaryNameDataGridViewTextBoxColumn
            // 
            this.customerGroupPrimaryNameDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Primary_Name";
            this.customerGroupPrimaryNameDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Primary_Name";
            this.customerGroupPrimaryNameDataGridViewTextBoxColumn.Name = "customerGroupPrimaryNameDataGridViewTextBoxColumn";
            // 
            // customerGroupPrimaryTelephoneDataGridViewTextBoxColumn
            // 
            this.customerGroupPrimaryTelephoneDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Primary_Telephone";
            this.customerGroupPrimaryTelephoneDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Primary_Telephone";
            this.customerGroupPrimaryTelephoneDataGridViewTextBoxColumn.Name = "customerGroupPrimaryTelephoneDataGridViewTextBoxColumn";
            // 
            // customerGroupPrimaryFaxDataGridViewTextBoxColumn
            // 
            this.customerGroupPrimaryFaxDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Primary_Fax";
            this.customerGroupPrimaryFaxDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Primary_Fax";
            this.customerGroupPrimaryFaxDataGridViewTextBoxColumn.Name = "customerGroupPrimaryFaxDataGridViewTextBoxColumn";
            // 
            // customerGroupPrimaryEmailDataGridViewTextBoxColumn
            // 
            this.customerGroupPrimaryEmailDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Primary_Email";
            this.customerGroupPrimaryEmailDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Primary_Email";
            this.customerGroupPrimaryEmailDataGridViewTextBoxColumn.Name = "customerGroupPrimaryEmailDataGridViewTextBoxColumn";
            // 
            // customerGroupSecondaryNameDataGridViewTextBoxColumn
            // 
            this.customerGroupSecondaryNameDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Secondary_Name";
            this.customerGroupSecondaryNameDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Secondary_Name";
            this.customerGroupSecondaryNameDataGridViewTextBoxColumn.Name = "customerGroupSecondaryNameDataGridViewTextBoxColumn";
            // 
            // customerGroupSecondaryTelephoneDataGridViewTextBoxColumn
            // 
            this.customerGroupSecondaryTelephoneDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Secondary_Telephone";
            this.customerGroupSecondaryTelephoneDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Secondary_Telephone";
            this.customerGroupSecondaryTelephoneDataGridViewTextBoxColumn.Name = "customerGroupSecondaryTelephoneDataGridViewTextBoxColumn";
            // 
            // customerGroupSecondaryFaxDataGridViewTextBoxColumn
            // 
            this.customerGroupSecondaryFaxDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Secondary_Fax";
            this.customerGroupSecondaryFaxDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Secondary_Fax";
            this.customerGroupSecondaryFaxDataGridViewTextBoxColumn.Name = "customerGroupSecondaryFaxDataGridViewTextBoxColumn";
            // 
            // customerGroupSecondaryEmailDataGridViewTextBoxColumn
            // 
            this.customerGroupSecondaryEmailDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Secondary_Email";
            this.customerGroupSecondaryEmailDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Secondary_Email";
            this.customerGroupSecondaryEmailDataGridViewTextBoxColumn.Name = "customerGroupSecondaryEmailDataGridViewTextBoxColumn";
            // 
            // customerGroupActiveDataGridViewCheckBoxColumn
            // 
            this.customerGroupActiveDataGridViewCheckBoxColumn.DataPropertyName = "Customer_Group_Active";
            this.customerGroupActiveDataGridViewCheckBoxColumn.HeaderText = "Customer_Group_Active";
            this.customerGroupActiveDataGridViewCheckBoxColumn.Name = "customerGroupActiveDataGridViewCheckBoxColumn";
            // 
            // customerGroupAccountNumberDataGridViewTextBoxColumn
            // 
            this.customerGroupAccountNumberDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Account_Number";
            this.customerGroupAccountNumberDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Account_Number";
            this.customerGroupAccountNumberDataGridViewTextBoxColumn.Name = "customerGroupAccountNumberDataGridViewTextBoxColumn";
            // 
            // customerGroupInvoiceOnlyAccountDataGridViewCheckBoxColumn
            // 
            this.customerGroupInvoiceOnlyAccountDataGridViewCheckBoxColumn.DataPropertyName = "Customer_Group_InvoiceOnly_Account";
            this.customerGroupInvoiceOnlyAccountDataGridViewCheckBoxColumn.HeaderText = "Customer_Group_InvoiceOnly_Account";
            this.customerGroupInvoiceOnlyAccountDataGridViewCheckBoxColumn.Name = "customerGroupInvoiceOnlyAccountDataGridViewCheckBoxColumn";
            // 
            // customerGroupIncludeForSalesVATDataGridViewCheckBoxColumn
            // 
            this.customerGroupIncludeForSalesVATDataGridViewCheckBoxColumn.DataPropertyName = "Customer_Group_Include_For_Sales_VAT";
            this.customerGroupIncludeForSalesVATDataGridViewCheckBoxColumn.HeaderText = "Customer_Group_Include_For_Sales_VAT";
            this.customerGroupIncludeForSalesVATDataGridViewCheckBoxColumn.Name = "customerGroupIncludeForSalesVATDataGridViewCheckBoxColumn";
            // 
            // customerGroupZeroRatedVatDataGridViewCheckBoxColumn
            // 
            this.customerGroupZeroRatedVatDataGridViewCheckBoxColumn.DataPropertyName = "Customer_Group_Zero_Rated_Vat";
            this.customerGroupZeroRatedVatDataGridViewCheckBoxColumn.HeaderText = "Customer_Group_Zero_Rated_Vat";
            this.customerGroupZeroRatedVatDataGridViewCheckBoxColumn.Name = "customerGroupZeroRatedVatDataGridViewCheckBoxColumn";
            // 
            // customerGroupAllowZeroValueItemsDataGridViewCheckBoxColumn
            // 
            this.customerGroupAllowZeroValueItemsDataGridViewCheckBoxColumn.DataPropertyName = "Customer_Group_Allow_Zero_Value_Items";
            this.customerGroupAllowZeroValueItemsDataGridViewCheckBoxColumn.HeaderText = "Customer_Group_Allow_Zero_Value_Items";
            this.customerGroupAllowZeroValueItemsDataGridViewCheckBoxColumn.Name = "customerGroupAllowZeroValueItemsDataGridViewCheckBoxColumn";
            // 
            // customerGroupOrderNumberRequiredDataGridViewCheckBoxColumn
            // 
            this.customerGroupOrderNumberRequiredDataGridViewCheckBoxColumn.DataPropertyName = "Customer_Group_Order_Number_Required";
            this.customerGroupOrderNumberRequiredDataGridViewCheckBoxColumn.HeaderText = "Customer_Group_Order_Number_Required";
            this.customerGroupOrderNumberRequiredDataGridViewCheckBoxColumn.Name = "customerGroupOrderNumberRequiredDataGridViewCheckBoxColumn";
            // 
            // policySteer1stDataGridViewTextBoxColumn
            // 
            this.policySteer1stDataGridViewTextBoxColumn.DataPropertyName = "Policy_Steer_1st";
            this.policySteer1stDataGridViewTextBoxColumn.HeaderText = "Policy_Steer_1st";
            this.policySteer1stDataGridViewTextBoxColumn.Name = "policySteer1stDataGridViewTextBoxColumn";
            // 
            // policySteer2ndDataGridViewTextBoxColumn
            // 
            this.policySteer2ndDataGridViewTextBoxColumn.DataPropertyName = "Policy_Steer_2nd";
            this.policySteer2ndDataGridViewTextBoxColumn.HeaderText = "Policy_Steer_2nd";
            this.policySteer2ndDataGridViewTextBoxColumn.Name = "policySteer2ndDataGridViewTextBoxColumn";
            // 
            // policyDrive1stDataGridViewTextBoxColumn
            // 
            this.policyDrive1stDataGridViewTextBoxColumn.DataPropertyName = "Policy_Drive_1st";
            this.policyDrive1stDataGridViewTextBoxColumn.HeaderText = "Policy_Drive_1st";
            this.policyDrive1stDataGridViewTextBoxColumn.Name = "policyDrive1stDataGridViewTextBoxColumn";
            // 
            // policyDrive2ndDataGridViewTextBoxColumn
            // 
            this.policyDrive2ndDataGridViewTextBoxColumn.DataPropertyName = "Policy_Drive_2nd";
            this.policyDrive2ndDataGridViewTextBoxColumn.HeaderText = "Policy_Drive_2nd";
            this.policyDrive2ndDataGridViewTextBoxColumn.Name = "policyDrive2ndDataGridViewTextBoxColumn";
            // 
            // policyTrailer1stDataGridViewTextBoxColumn
            // 
            this.policyTrailer1stDataGridViewTextBoxColumn.DataPropertyName = "Policy_Trailer_1st";
            this.policyTrailer1stDataGridViewTextBoxColumn.HeaderText = "Policy_Trailer_1st";
            this.policyTrailer1stDataGridViewTextBoxColumn.Name = "policyTrailer1stDataGridViewTextBoxColumn";
            // 
            // policyTrailer2ndDataGridViewTextBoxColumn
            // 
            this.policyTrailer2ndDataGridViewTextBoxColumn.DataPropertyName = "Policy_Trailer_2nd";
            this.policyTrailer2ndDataGridViewTextBoxColumn.HeaderText = "Policy_Trailer_2nd";
            this.policyTrailer2ndDataGridViewTextBoxColumn.Name = "policyTrailer2ndDataGridViewTextBoxColumn";
            // 
            // rdPartyCustomerNamePromptDataGridViewCheckBoxColumn
            // 
            this.rdPartyCustomerNamePromptDataGridViewCheckBoxColumn.DataPropertyName = "3rdPartyCustomerNamePrompt";
            this.rdPartyCustomerNamePromptDataGridViewCheckBoxColumn.HeaderText = "3rdPartyCustomerNamePrompt";
            this.rdPartyCustomerNamePromptDataGridViewCheckBoxColumn.Name = "rdPartyCustomerNamePromptDataGridViewCheckBoxColumn";
            // 
            // customerGroupPricingRenewalDateDataGridViewTextBoxColumn
            // 
            this.customerGroupPricingRenewalDateDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Pricing_Renewal_Date";
            this.customerGroupPricingRenewalDateDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Pricing_Renewal_Date";
            this.customerGroupPricingRenewalDateDataGridViewTextBoxColumn.Name = "customerGroupPricingRenewalDateDataGridViewTextBoxColumn";
            // 
            // customerGroupDaysBetweenInspectionsDataGridViewTextBoxColumn
            // 
            this.customerGroupDaysBetweenInspectionsDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Days_Between_Inspections";
            this.customerGroupDaysBetweenInspectionsDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Days_Between_Inspections";
            this.customerGroupDaysBetweenInspectionsDataGridViewTextBoxColumn.Name = "customerGroupDaysBetweenInspectionsDataGridViewTextBoxColumn";
            // 
            // customerGroupPaymentDaysDataGridViewTextBoxColumn
            // 
            this.customerGroupPaymentDaysDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Payment_Days";
            this.customerGroupPaymentDaysDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Payment_Days";
            this.customerGroupPaymentDaysDataGridViewTextBoxColumn.Name = "customerGroupPaymentDaysDataGridViewTextBoxColumn";
            // 
            // customerGroupOrderNumberRequestedAtICDataGridViewCheckBoxColumn
            // 
            this.customerGroupOrderNumberRequestedAtICDataGridViewCheckBoxColumn.DataPropertyName = "Customer_Group_Order_Number_Requested_At_IC";
            this.customerGroupOrderNumberRequestedAtICDataGridViewCheckBoxColumn.HeaderText = "Customer_Group_Order_Number_Requested_At_IC";
            this.customerGroupOrderNumberRequestedAtICDataGridViewCheckBoxColumn.Name = "customerGroupOrderNumberRequestedAtICDataGridViewCheckBoxColumn";
            // 
            // salesmanInitialsDataGridViewTextBoxColumn
            // 
            this.salesmanInitialsDataGridViewTextBoxColumn.DataPropertyName = "Salesman_Initials";
            this.salesmanInitialsDataGridViewTextBoxColumn.HeaderText = "Salesman_Initials";
            this.salesmanInitialsDataGridViewTextBoxColumn.Name = "salesmanInitialsDataGridViewTextBoxColumn";
            // 
            // useGLPBuyingPricesDataGridViewCheckBoxColumn
            // 
            this.useGLPBuyingPricesDataGridViewCheckBoxColumn.DataPropertyName = "Use_GLP_Buying_Prices";
            this.useGLPBuyingPricesDataGridViewCheckBoxColumn.HeaderText = "Use_GLP_Buying_Prices";
            this.useGLPBuyingPricesDataGridViewCheckBoxColumn.Name = "useGLPBuyingPricesDataGridViewCheckBoxColumn";
            // 
            // showToServiceProvidersDataGridViewCheckBoxColumn
            // 
            this.showToServiceProvidersDataGridViewCheckBoxColumn.DataPropertyName = "Show_To_Service_Providers";
            this.showToServiceProvidersDataGridViewCheckBoxColumn.HeaderText = "Show_To_Service_Providers";
            this.showToServiceProvidersDataGridViewCheckBoxColumn.Name = "showToServiceProvidersDataGridViewCheckBoxColumn";
            // 
            // isBankFinancedDataGridViewCheckBoxColumn
            // 
            this.isBankFinancedDataGridViewCheckBoxColumn.DataPropertyName = "Is_Bank_Financed";
            this.isBankFinancedDataGridViewCheckBoxColumn.HeaderText = "Is_Bank_Financed";
            this.isBankFinancedDataGridViewCheckBoxColumn.Name = "isBankFinancedDataGridViewCheckBoxColumn";
            // 
            // majorRepairDestDataGridViewTextBoxColumn
            // 
            this.majorRepairDestDataGridViewTextBoxColumn.DataPropertyName = "Major_Repair_Dest";
            this.majorRepairDestDataGridViewTextBoxColumn.HeaderText = "Major_Repair_Dest";
            this.majorRepairDestDataGridViewTextBoxColumn.Name = "majorRepairDestDataGridViewTextBoxColumn";
            // 
            // forDeteDataGridViewCheckBoxColumn
            // 
            this.forDeteDataGridViewCheckBoxColumn.DataPropertyName = "For_Dete";
            this.forDeteDataGridViewCheckBoxColumn.HeaderText = "For_Dete";
            this.forDeteDataGridViewCheckBoxColumn.Name = "forDeteDataGridViewCheckBoxColumn";
            // 
            // customerGroupCreditLimitDataGridViewTextBoxColumn
            // 
            this.customerGroupCreditLimitDataGridViewTextBoxColumn.DataPropertyName = "Customer_Group_Credit_Limit";
            this.customerGroupCreditLimitDataGridViewTextBoxColumn.HeaderText = "Customer_Group_Credit_Limit";
            this.customerGroupCreditLimitDataGridViewTextBoxColumn.Name = "customerGroupCreditLimitDataGridViewTextBoxColumn";
            // 
            // forArchiveDataGridViewCheckBoxColumn
            // 
            this.forArchiveDataGridViewCheckBoxColumn.DataPropertyName = "For_Archive";
            this.forArchiveDataGridViewCheckBoxColumn.HeaderText = "For_Archive";
            this.forArchiveDataGridViewCheckBoxColumn.Name = "forArchiveDataGridViewCheckBoxColumn";
            // 
            // retorqueTextDataGridViewTextBoxColumn
            // 
            this.retorqueTextDataGridViewTextBoxColumn.DataPropertyName = "Retorque_Text";
            this.retorqueTextDataGridViewTextBoxColumn.HeaderText = "Retorque_Text";
            this.retorqueTextDataGridViewTextBoxColumn.Name = "retorqueTextDataGridViewTextBoxColumn";
            // 
            // retorqueReminderActiveDataGridViewCheckBoxColumn
            // 
            this.retorqueReminderActiveDataGridViewCheckBoxColumn.DataPropertyName = "Retorque_Reminder_Active";
            this.retorqueReminderActiveDataGridViewCheckBoxColumn.HeaderText = "Retorque_Reminder_Active";
            this.retorqueReminderActiveDataGridViewCheckBoxColumn.Name = "retorqueReminderActiveDataGridViewCheckBoxColumn";
            // 
            // affilliateLogoDataGridViewTextBoxColumn
            // 
            this.affilliateLogoDataGridViewTextBoxColumn.DataPropertyName = "Affilliate_Logo";
            this.affilliateLogoDataGridViewTextBoxColumn.HeaderText = "Affilliate_Logo";
            this.affilliateLogoDataGridViewTextBoxColumn.Name = "affilliateLogoDataGridViewTextBoxColumn";
            // 
            // managedFleetDataGridViewCheckBoxColumn
            // 
            this.managedFleetDataGridViewCheckBoxColumn.DataPropertyName = "Managed_Fleet";
            this.managedFleetDataGridViewCheckBoxColumn.HeaderText = "Managed_Fleet";
            this.managedFleetDataGridViewCheckBoxColumn.Name = "managedFleetDataGridViewCheckBoxColumn";
            // 
            // cmsGrid
            // 
            this.cmsGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miExport});
            this.cmsGrid.Name = "cmsGrid";
            this.cmsGrid.Size = new System.Drawing.Size(148, 26);
            // 
            // miExport
            // 
            this.miExport.Name = "miExport";
            this.miExport.Size = new System.Drawing.Size(147, 22);
            this.miExport.Text = "Export To CSV";
            this.miExport.Click += new System.EventHandler(this.miExport_Click);
            // 
            // tpUsers
            // 
            this.tpUsers.Location = new System.Drawing.Point(4, 29);
            this.tpUsers.Name = "tpUsers";
            this.tpUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tpUsers.Size = new System.Drawing.Size(1312, 685);
            this.tpUsers.TabIndex = 2;
            this.tpUsers.Text = "Users";
            this.tpUsers.UseVisualStyleBackColor = true;
            // 
            // tcAdmin
            // 
            this.tcAdmin.Controls.Add(this.tpUsers);
            this.tcAdmin.Controls.Add(this.tpCustomers);
            this.tcAdmin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcAdmin.Location = new System.Drawing.Point(0, 0);
            this.tcAdmin.Name = "tcAdmin";
            this.tcAdmin.SelectedIndex = 0;
            this.tcAdmin.Size = new System.Drawing.Size(1320, 718);
            this.tcAdmin.TabIndex = 0;
            this.tcAdmin.SelectedIndexChanged += new System.EventHandler(this.tcAdmin_SelectedIndexChanged);
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(1320, 718);
            this.Controls.Add(this.tcAdmin);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmAdmin";
            this.Text = "frmAdmin";
            this.Load += new System.EventHandler(this.frmAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCustomerLocationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCustomerGroupsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet5)).EndInit();
            this.tpCustomers.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvGroups)).EndInit();
            this.cmsGrid.ResumeLayout(false);
            this.tcAdmin.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private smartdevDataSet5 smartdevDataSet5;
        private System.Windows.Forms.BindingSource uspSMARTgetCustomerGroupsBindingSource;
        private smartdevDataSet5TableAdapters.usp_SMART_getCustomerGroupsTableAdapter usp_SMART_getCustomerGroupsTableAdapter;
        private smartdevDataSet6 smartdevDataSet6;
        private System.Windows.Forms.BindingSource uspSMARTgetCustomerLocationsBindingSource;
        private smartdevDataSet6TableAdapters.usp_SMART_getCustomerLocationsTableAdapter usp_SMART_getCustomerLocationsTableAdapter;
        private System.Windows.Forms.TabPage tpCustomers;
        private System.Windows.Forms.DataGridView dgvLocs;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupIDDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocAddress1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocAddress2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocAddress3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocAddress4DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocCountyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocPostcode1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocPostcode2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocCountryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocPrimaryContactDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocSecondaryContactDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocTelephoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocFaxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLocEmailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceProviderLocIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customerLocActiveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvAddress1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvAddress2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvAddress3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvAddress4DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvCountyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvPostcode1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvPostcode2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerInvCountryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastVisitDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastVisitByDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn casingsEnterCasingCollectionChainDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn forArchiveDataGridViewCheckBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn alertIllegalRemovalDepthEmailToDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn notificationsEmailDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn notificationsTextDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridView dgvGroups;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerSuperGroupIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupAddress1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupAddress2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupAddress3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupAddress4DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupCountyDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupPostcode1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupPostcode2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupCountryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupPrimaryNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupPrimaryTelephoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupPrimaryFaxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupPrimaryEmailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupSecondaryNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupSecondaryTelephoneDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupSecondaryFaxDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupSecondaryEmailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customerGroupActiveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupAccountNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customerGroupInvoiceOnlyAccountDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customerGroupIncludeForSalesVATDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customerGroupZeroRatedVatDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customerGroupAllowZeroValueItemsDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customerGroupOrderNumberRequiredDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policySteer1stDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policySteer2ndDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policyDrive1stDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policyDrive2ndDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policyTrailer1stDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn policyTrailer2ndDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn rdPartyCustomerNamePromptDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupPricingRenewalDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupDaysBetweenInspectionsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupPaymentDaysDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn customerGroupOrderNumberRequestedAtICDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn salesmanInitialsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn useGLPBuyingPricesDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn showToServiceProvidersDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isBankFinancedDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn majorRepairDestDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn forDeteDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerGroupCreditLimitDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn forArchiveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn retorqueTextDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn retorqueReminderActiveDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn affilliateLogoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn managedFleetDataGridViewCheckBoxColumn;
        private System.Windows.Forms.TabPage tpUsers;
        private System.Windows.Forms.TabControl tcAdmin;
        private System.Windows.Forms.ContextMenuStrip cmsGrid;
        private System.Windows.Forms.ToolStripMenuItem miExport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ProgressBar pbExport;
    }
}