﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Smart
{
    public partial class frmMaint : DevExpress.XtraBars.TabForm
    {
        public frmMaint()
        {
            InitializeComponent();
        }


        private void UploadDocument(string reg, string fileName, string fileFrom)
        {

            // check if folder exists
            //string path = @"ftp://10.2.11.160/devVehicleDocs/
            WebRequest request = WebRequest.Create("ftp://10.2.11.160/devVehicleDocs/" + reg + "/");
            request.Method = WebRequestMethods.Ftp.MakeDirectory;
            request.Credentials = new NetworkCredential("devdocsupload", "U25SeN");
            try
            {
                request.GetResponse();
            }
            catch (Exception e)
            {
                //directory exists
            }

            FtpWebRequest reqFTP;
            FileInfo fileInf = new FileInfo(fileFrom);
            string uri = "ftp://10.2.11.160/VehicleDocs/" + reg + "/" + fileInf.Name;

            reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://10.2.11.160/devVehicleDocs/" + reg + "/" + fileInf.Name));
            reqFTP.Credentials = new NetworkCredential("devdocsupload", "U25SeN");

            //            reqFTP.Credentials = new NetworkCredential("docsupload", "3ZmxJR");

            reqFTP.KeepAlive = false;

            reqFTP.Method = WebRequestMethods.Ftp.UploadFile;

            reqFTP.UseBinary = true;

            reqFTP.ContentLength = fileInf.Length;

            int buffLength = 2048;
            byte[] buff = new byte[buffLength];
            int contentLen;

            FileStream fs = fileInf.OpenRead();

            try
            {
                Stream strm = reqFTP.GetRequestStream();

                contentLen = fs.Read(buff, 0, buffLength);

                while (contentLen != 0)
                {
                    strm.Write(buff, 0, contentLen);
                    contentLen = fs.Read(buff, 0, buffLength);
                }

                strm.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "UploadDocuemnt Error");
            }
        }

        private void label2_DragEnter(object sender, DragEventArgs e)
        {
            Cursor.Current = Cursors.Hand;
            e.Effect = DragDropEffects.Copy;

        }

        private void label2_DragDrop(object sender, DragEventArgs e)
        {
            string[] fileNames = null;


            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                fileNames = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string fileName in fileNames)
                {
                }
            }
            else if (e.Data.GetDataPresent("FileGroupDescriptor"))
            {
                object s = e.Data.GetData("FileGroupDescriptor");

                Stream theStream = (Stream)e.Data.GetData("FileGroupDescriptor");
                byte[] fileGroupDescriptor = new byte[512];
                theStream.Read(fileGroupDescriptor, 0, 512);

                StringBuilder fileName = new StringBuilder("");

                for (int i = 76; fileGroupDescriptor[i] != 0; i++)
                { fileName.Append(Convert.ToChar(fileGroupDescriptor[i])); }

                string theFile = fileName.ToString();
                String fileName1 = System.IO.Path.GetFileName(theFile);

                // get the actual raw file into memory
                MemoryStream ms = (MemoryStream)e.Data.GetData("FileContents", true);
                // allocate enough bytes to hold the raw data
                byte[] fileBytes = new byte[ms.Length];
                // set starting position at first byte and read in the raw data
                ms.Position = 0;
                ms.Read(fileBytes, 0, (int)ms.Length);
                // create a file and save the raw zip file to it
                FileStream fs = new FileStream(theFile, FileMode.Create);
                fs.Write(fileBytes, 0, (int)fileBytes.Length);

                fs.Close();  // close the file

                FileInfo tempFile = new FileInfo(theFile);

                tempFile.CopyTo(@"c:\smart\_" + label2.Tag + "_REG.pdf");
                // always good to make sure we actually created the file
                if (tempFile.Exists == true)
                {
                    // for now, just delete what we created
                    tempFile.Delete();
                }
                else
                {
                    //    Trace.WriteLine("File was not created!");
                }
            }


        }

        private void label2_DragLeave(object sender, EventArgs e)
        {
        }

        private void pb_DragEnter(object sender, DragEventArgs e)
        {
            Cursor.Current = Cursors.Hand;
            e.Effect = DragDropEffects.Copy;

        }

        private void pictureBox1_DragEnter(object sender, DragEventArgs e)
        {

        }

        private void pb_DragDrop(object sender, DragEventArgs e)
        {
            string[] fileNames = null;


            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                fileNames = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string fileName in fileNames)
                {
                }
            }
            else if (e.Data.GetDataPresent("FileGroupDescriptor"))
            {
                object s = e.Data.GetData("FileGroupDescriptor");

                Stream theStream = (Stream)e.Data.GetData("FileGroupDescriptor");
                byte[] fileGroupDescriptor = new byte[512];
                theStream.Read(fileGroupDescriptor, 0, 512);

                StringBuilder fileName = new StringBuilder("");

                for (int i = 76; fileGroupDescriptor[i] != 0; i++)
                { fileName.Append(Convert.ToChar(fileGroupDescriptor[i])); }

                string theFile = fileName.ToString();
                String fileName1 = System.IO.Path.GetFileName(theFile);

                // get the actual raw file into memory
                MemoryStream ms = (MemoryStream)e.Data.GetData("FileContents", true);
                // allocate enough bytes to hold the raw data
                byte[] fileBytes = new byte[ms.Length];
                // set starting position at first byte and read in the raw data
                ms.Position = 0;
                ms.Read(fileBytes, 0, (int)ms.Length);
                // create a file and save the raw zip file to it
                FileStream fs = new FileStream(theFile, FileMode.Create);
                fs.Write(fileBytes, 0, (int)fileBytes.Length);

                fs.Close();  // close the file

                FileInfo tempFile = new FileInfo(theFile);
                string dateToUse = "";

                foreach (Control c in pnlDetails.Controls)
                {
                    if ((c is DateTimePicker ) && (c.Tag == ((PictureBox)sender).Tag))
                    {
                        dateToUse = ((DateTimePicker)c).Value.ToShortDateString();
                    }
                }

//                string newFileName = ((PictureBox)sender).Tag + "_" + cboReg.Text + "_" + DateTime.Today.ToShortDateString().Replace("/", ".") + tempFile.Extension;
                string newFileName = ((PictureBox)sender).Tag + "_" + cboReg.Text + "_" + dateToUse.Replace("/", ".") + tempFile.Extension;
                tempFile.CopyTo(@"c:\smart\" + newFileName, true);
                // always good to make sure we actually created the file
                if (tempFile.Exists == true)
                {
                    // upload it, then delete the temp version
                    UploadDocument(cboReg.Text,
                                            newFileName,
                                            @"c:\smart\" + newFileName);
                    tempFile.Delete();
                }
                else
                {
                    //    Trace.WriteLine("File was not created!");
                }
                ((Control)sender).AllowDrop = false;
            }

        }


        private void pictureBox1_DragDrop(object sender, DragEventArgs e)
        {


        }

        private void tpPhotos_Click(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
        }

        private void control_MouseMove(object sender, MouseEventArgs e)
        {

            Control control = (Control)sender;
            PictureBox pic = (PictureBox)control;
            pictureBox2.Image = pic.Image;

        }

        private void tabPage6_Click(object sender, EventArgs e)
        {


        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Name == "tabPage6")
            {
                Cursor.Current = Cursors.WaitCursor;
                int sizeWidth = 100;
                int sizeHeight = 100;
                int locX = 20;
                int locY = 10;

                void loadImagestoPanel(String imageName, String ImageFullName, int newLocX, int newLocY)
                {
                    PictureBox ctrl = new PictureBox();
                    ctrl.Image = Image.FromFile(ImageFullName);
                    ctrl.BackColor = Color.Black;
                    ctrl.Location = new Point(newLocX, newLocY);
                    ctrl.Size = new System.Drawing.Size(sizeWidth, sizeHeight);
                    ctrl.SizeMode = PictureBoxSizeMode.StretchImage;
                    ctrl.MouseMove += new MouseEventHandler(control_MouseMove);
                    pnlImages.Controls.Add(ctrl);


                }

                DirectoryInfo Folder;
                FileInfo[] Images;
                Folder = new DirectoryInfo(@"c:\smart");
                Images = Folder.GetFiles();
                pnlImages.Controls.Clear();
                int locnewX = locX;
                int locnewY = locY;


                foreach (FileInfo img in Images)
                {
                    if (img.Extension.ToLower() == ".png" || img.Extension.ToLower() == ".jpg" || img.Extension.ToLower() == ".gif" || img.Extension.ToLower() == ".jpeg" || img.Extension.ToLower() == ".bmp" || img.Extension.ToLower() == ".tif")
                    {

                        if (locnewX >= pnlImages.Width - sizeWidth - 10)
                        {
                            locnewX = locX;
                            locY = locY + sizeHeight + 30;
                            locnewY = locY;
                        }
                        else
                        {

                            locnewY = locY;
                        }

                        loadImagestoPanel(img.Name, img.FullName, locnewX, locnewY);
                        locnewY = locY + sizeHeight + 10;
                        locnewX = locnewX + sizeWidth + 10;

                    }


                }
                }
            Cursor.Current = Cursors.Default;
        }

        private void rotateToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void clockwiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Image flipImage = pictureBox2.Image;
            flipImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
            pictureBox2.Image = flipImage;

        }

        private void anticlockwiseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Image flipImage = pictureBox2.Image;
            flipImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
            pictureBox2.Image = flipImage;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Image flipImage = pictureBox2.Image;
            flipImage.RotateFlip(RotateFlipType.Rotate180FlipNone);
            pictureBox2.Image = flipImage;

        }

        private void frmMaint_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'smartdevDataSet4.usp_SMARTgetAllVehicles' table. You can move, or remove it, as needed.
            this.usp_SMARTgetAllVehiclesTableAdapter.Fill(this.smartdevDataSet4.usp_SMARTgetAllVehicles);
            // TODO: This line of code loads data into the 'ramsdevDataSet3.usp_SMART_getVehiclesForCustomer' table. You can move, or remove it, as needed.
            this.usp_SMART_getVehiclesForCustomerTableAdapter.Fill(this.ramsdevDataSet3.usp_SMART_getVehiclesForCustomer);

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void comboBox1_DropDownClosed(object sender, EventArgs e)
        {

        }

        private void tpDocs_Enter(object sender, EventArgs e)
        {

        }

        private void dtpValueChanged(object sender, EventArgs e)
        {
            if (((DateTimePicker)sender).Checked)
            {
                foreach (Control c in pnlDetails.Controls)
                {
                    if (c.Tag == ((DateTimePicker)sender).Tag)
                    {
                        c.AllowDrop = true;
                    }
                }
            }

            if (((DateTimePicker)sender).Checked)
            {
                ((DateTimePicker)sender).Format = DateTimePickerFormat.Short;
            }
            else
            {
                ((DateTimePicker)sender).Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            }

        } // dtpValueChanged
    }
}
