﻿namespace Smart
{
    partial class frmMaint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMaint));
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.cboReg = new System.Windows.Forms.ComboBox();
            this.uspSMARTgetAllVehiclesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.smartdevDataSet4 = new Smart.smartdevDataSet4();
            this.label6 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tpDocs = new System.Windows.Forms.TabPage();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.dateTimePicker10 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker9 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker8 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker7 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.dtpMOT = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlLeft = new System.Windows.Forms.Panel();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.rotateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clockwiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.anticlockwiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlImages = new System.Windows.Forms.Panel();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.tpBreakdown = new System.Windows.Forms.TabPage();
            this.ramsdevDataSet3 = new Smart.ramsdevDataSet3();
            this.uspSMARTgetVehiclesForCustomerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usp_SMART_getVehiclesForCustomerTableAdapter = new Smart.ramsdevDataSet3TableAdapters.usp_SMART_getVehiclesForCustomerTableAdapter();
            this.smartdevDataSet3 = new Smart.smartdevDataSet3();
            this.uspSMARTgetVehicleDetailsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usp_SMARTgetVehicleDetailsTableAdapter = new Smart.smartdevDataSet3TableAdapters.usp_SMARTgetVehicleDetailsTableAdapter();
            this.usp_SMARTgetAllVehiclesTableAdapter = new Smart.smartdevDataSet4TableAdapters.usp_SMARTgetAllVehiclesTableAdapter();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetAllVehiclesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet4)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tpDocs.SuspendLayout();
            this.pnlDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetVehiclesForCustomerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetVehicleDetailsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.Controls.Add(this.cboReg);
            this.pnlHeader.Controls.Add(this.label6);
            this.pnlHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(1372, 154);
            this.pnlHeader.TabIndex = 0;
            // 
            // cboReg
            // 
            this.cboReg.DataSource = this.uspSMARTgetAllVehiclesBindingSource;
            this.cboReg.DisplayMember = "Customer_Fleetlist_VehicleReg";
            this.cboReg.FormattingEnabled = true;
            this.cboReg.Location = new System.Drawing.Point(161, 33);
            this.cboReg.MaxDropDownItems = 20;
            this.cboReg.Name = "cboReg";
            this.cboReg.Size = new System.Drawing.Size(260, 28);
            this.cboReg.TabIndex = 1;
            this.cboReg.ValueMember = "Customer_FleetListID";
            this.cboReg.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.cboReg.DropDownClosed += new System.EventHandler(this.comboBox1_DropDownClosed);
            // 
            // uspSMARTgetAllVehiclesBindingSource
            // 
            this.uspSMARTgetAllVehiclesBindingSource.DataMember = "usp_SMARTgetAllVehicles";
            this.uspSMARTgetAllVehiclesBindingSource.DataSource = this.smartdevDataSet4;
            // 
            // smartdevDataSet4
            // 
            this.smartdevDataSet4.DataSetName = "smartdevDataSet4";
            this.smartdevDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(48, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Registration";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tpDocs);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tpBreakdown);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 154);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1372, 511);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage1.Size = new System.Drawing.Size(1364, 478);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Rental";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage2.Size = new System.Drawing.Size(1364, 478);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Maintenance";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage3.Size = new System.Drawing.Size(1364, 478);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Compliance";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage4.Size = new System.Drawing.Size(1364, 478);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Accounts";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Location = new System.Drawing.Point(4, 29);
            this.tabPage5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabPage5.Size = new System.Drawing.Size(1364, 478);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Reports";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tpDocs
            // 
            this.tpDocs.Controls.Add(this.pnlDetails);
            this.tpDocs.Controls.Add(this.pnlLeft);
            this.tpDocs.Location = new System.Drawing.Point(4, 29);
            this.tpDocs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpDocs.Name = "tpDocs";
            this.tpDocs.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpDocs.Size = new System.Drawing.Size(1364, 478);
            this.tpDocs.TabIndex = 5;
            this.tpDocs.Text = "Documents";
            this.tpDocs.UseVisualStyleBackColor = true;
            this.tpDocs.Enter += new System.EventHandler(this.tpDocs_Enter);
            // 
            // pnlDetails
            // 
            this.pnlDetails.Controls.Add(this.pictureBox12);
            this.pnlDetails.Controls.Add(this.pictureBox11);
            this.pnlDetails.Controls.Add(this.pictureBox10);
            this.pnlDetails.Controls.Add(this.pictureBox9);
            this.pnlDetails.Controls.Add(this.pictureBox8);
            this.pnlDetails.Controls.Add(this.pictureBox7);
            this.pnlDetails.Controls.Add(this.pictureBox6);
            this.pnlDetails.Controls.Add(this.pictureBox5);
            this.pnlDetails.Controls.Add(this.pictureBox4);
            this.pnlDetails.Controls.Add(this.pictureBox3);
            this.pnlDetails.Controls.Add(this.dateTimePicker10);
            this.pnlDetails.Controls.Add(this.dateTimePicker9);
            this.pnlDetails.Controls.Add(this.dateTimePicker8);
            this.pnlDetails.Controls.Add(this.dateTimePicker7);
            this.pnlDetails.Controls.Add(this.dateTimePicker6);
            this.pnlDetails.Controls.Add(this.dateTimePicker5);
            this.pnlDetails.Controls.Add(this.dateTimePicker4);
            this.pnlDetails.Controls.Add(this.dateTimePicker3);
            this.pnlDetails.Controls.Add(this.dateTimePicker2);
            this.pnlDetails.Controls.Add(this.dateTimePicker1);
            this.pnlDetails.Controls.Add(this.label15);
            this.pnlDetails.Controls.Add(this.label14);
            this.pnlDetails.Controls.Add(this.label13);
            this.pnlDetails.Controls.Add(this.label12);
            this.pnlDetails.Controls.Add(this.label11);
            this.pnlDetails.Controls.Add(this.label10);
            this.pnlDetails.Controls.Add(this.label9);
            this.pnlDetails.Controls.Add(this.label8);
            this.pnlDetails.Controls.Add(this.label7);
            this.pnlDetails.Controls.Add(this.button1);
            this.pnlDetails.Controls.Add(this.label5);
            this.pnlDetails.Controls.Add(this.label4);
            this.pnlDetails.Controls.Add(this.label3);
            this.pnlDetails.Controls.Add(this.pictureBox1);
            this.pnlDetails.Controls.Add(this.dtpMOT);
            this.pnlDetails.Controls.Add(this.label2);
            this.pnlDetails.Controls.Add(this.label1);
            this.pnlDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDetails.Location = new System.Drawing.Point(257, 5);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(1103, 468);
            this.pnlDetails.TabIndex = 1;
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox12.Image")));
            this.pictureBox12.Location = new System.Drawing.Point(479, 31);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(32, 26);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 36;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Tag = "INSPECTION";
            this.pictureBox12.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox12.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox11.Image")));
            this.pictureBox11.Location = new System.Drawing.Point(479, 61);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(32, 26);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 35;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Tag = "LOLER";
            this.pictureBox11.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox11.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(479, 121);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(32, 26);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 34;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Tag = "RPC";
            this.pictureBox10.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox10.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox9.Image")));
            this.pictureBox9.Location = new System.Drawing.Point(479, 151);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(32, 26);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 33;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Tag = "TACHO";
            this.pictureBox9.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox9.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(479, 181);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(32, 26);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 32;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Tag = "VED";
            this.pictureBox8.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox8.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(479, 211);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(32, 26);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 31;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Tag = "V5";
            this.pictureBox7.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox7.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(479, 240);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(32, 26);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 30;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Tag = "PLATING";
            this.pictureBox6.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox6.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(479, 272);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(32, 26);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 29;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox5.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(479, 303);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(32, 26);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 28;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Tag = "BINSP";
            this.pictureBox4.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox4.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(479, 335);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(32, 26);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 27;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Tag = "RENTAL";
            this.pictureBox3.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox3.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // dateTimePicker10
            // 
            this.dateTimePicker10.Checked = false;
            this.dateTimePicker10.CustomFormat = " ";
            this.dateTimePicker10.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker10.Location = new System.Drawing.Point(243, 335);
            this.dateTimePicker10.Name = "dateTimePicker10";
            this.dateTimePicker10.ShowCheckBox = true;
            this.dateTimePicker10.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker10.TabIndex = 26;
            this.dateTimePicker10.Tag = "RENTAL";
            this.dateTimePicker10.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker9
            // 
            this.dateTimePicker9.Checked = false;
            this.dateTimePicker9.CustomFormat = " ";
            this.dateTimePicker9.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker9.Location = new System.Drawing.Point(243, 303);
            this.dateTimePicker9.Name = "dateTimePicker9";
            this.dateTimePicker9.ShowCheckBox = true;
            this.dateTimePicker9.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker9.TabIndex = 25;
            this.dateTimePicker9.Tag = "BINSP";
            this.dateTimePicker9.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker8
            // 
            this.dateTimePicker8.Checked = false;
            this.dateTimePicker8.CustomFormat = " ";
            this.dateTimePicker8.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker8.Location = new System.Drawing.Point(243, 240);
            this.dateTimePicker8.Name = "dateTimePicker8";
            this.dateTimePicker8.ShowCheckBox = true;
            this.dateTimePicker8.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker8.TabIndex = 24;
            this.dateTimePicker8.Tag = "PLATING";
            this.dateTimePicker8.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker7
            // 
            this.dateTimePicker7.Checked = false;
            this.dateTimePicker7.CustomFormat = " ";
            this.dateTimePicker7.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker7.Location = new System.Drawing.Point(243, 272);
            this.dateTimePicker7.Name = "dateTimePicker7";
            this.dateTimePicker7.ShowCheckBox = true;
            this.dateTimePicker7.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker7.TabIndex = 23;
            this.dateTimePicker7.Tag = "PDI";
            this.dateTimePicker7.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Checked = false;
            this.dateTimePicker6.CustomFormat = " ";
            this.dateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker6.Location = new System.Drawing.Point(243, 211);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.ShowCheckBox = true;
            this.dateTimePicker6.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker6.TabIndex = 22;
            this.dateTimePicker6.Tag = "V5";
            this.dateTimePicker6.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Checked = false;
            this.dateTimePicker5.CustomFormat = " ";
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker5.Location = new System.Drawing.Point(243, 181);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.ShowCheckBox = true;
            this.dateTimePicker5.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker5.TabIndex = 21;
            this.dateTimePicker5.Tag = "VED";
            this.dateTimePicker5.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Checked = false;
            this.dateTimePicker4.CustomFormat = " ";
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker4.Location = new System.Drawing.Point(243, 151);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.ShowCheckBox = true;
            this.dateTimePicker4.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker4.TabIndex = 20;
            this.dateTimePicker4.Tag = "TACHO";
            this.dateTimePicker4.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Checked = false;
            this.dateTimePicker3.CustomFormat = " ";
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker3.Location = new System.Drawing.Point(243, 121);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.ShowCheckBox = true;
            this.dateTimePicker3.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker3.TabIndex = 19;
            this.dateTimePicker3.Tag = "RPC";
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Checked = false;
            this.dateTimePicker2.CustomFormat = " ";
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(243, 31);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.ShowCheckBox = true;
            this.dateTimePicker2.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker2.TabIndex = 18;
            this.dateTimePicker2.Tag = "INSPECTION";
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Checked = false;
            this.dateTimePicker1.CustomFormat = " ";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(243, 61);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowCheckBox = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(143, 26);
            this.dateTimePicker1.TabIndex = 17;
            this.dateTimePicker1.Tag = "LOLER";
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(41, 338);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 20);
            this.label15.TabIndex = 16;
            this.label15.Text = "Rental";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(41, 306);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(123, 20);
            this.label14.TabIndex = 15;
            this.label14.Text = "Body Inspection";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(41, 275);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(36, 20);
            this.label13.TabIndex = 14;
            this.label13.Text = "PDI";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(41, 243);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 20);
            this.label12.TabIndex = 13;
            this.label12.Text = "Plating";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(41, 214);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 20);
            this.label11.TabIndex = 12;
            this.label11.Text = "V5";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(41, 184);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 20);
            this.label10.TabIndex = 11;
            this.label10.Text = "VED";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(41, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 20);
            this.label9.TabIndex = 10;
            this.label9.Text = "Tacho";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(41, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 20);
            this.label8.TabIndex = 9;
            this.label8.Text = "RPC";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(41, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 20);
            this.label7.TabIndex = 8;
            this.label7.Text = "LOLER";
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button1.Image = global::Smart.Properties.Resources.up_arrow_green_small_hi1;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(731, 85);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 26);
            this.button1.TabIndex = 7;
            this.button1.Text = "Upload";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(475, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(214, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Drag and Drop File Below";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(41, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Document Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(243, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Valid Until";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(479, 91);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Tag = "MOT";
            this.pictureBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.pb_DragDrop);
            this.pictureBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.pb_DragEnter);
            // 
            // dtpMOT
            // 
            this.dtpMOT.Checked = false;
            this.dtpMOT.CustomFormat = " ";
            this.dtpMOT.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMOT.Location = new System.Drawing.Point(243, 91);
            this.dtpMOT.Name = "dtpMOT";
            this.dtpMOT.ShowCheckBox = true;
            this.dtpMOT.Size = new System.Drawing.Size(143, 26);
            this.dtpMOT.TabIndex = 2;
            this.dtpMOT.Tag = "MOT";
            this.dtpMOT.ValueChanged += new System.EventHandler(this.dtpValueChanged);
            // 
            // label2
            // 
            this.label2.AllowDrop = true;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 20);
            this.label2.TabIndex = 1;
            this.label2.Tag = "MOT";
            this.label2.Text = "MOT";
            this.label2.DragDrop += new System.Windows.Forms.DragEventHandler(this.label2_DragDrop);
            this.label2.DragEnter += new System.Windows.Forms.DragEventHandler(this.label2_DragEnter);
            this.label2.DragLeave += new System.EventHandler(this.label2_DragLeave);
            // 
            // label1
            // 
            this.label1.AllowDrop = true;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 20);
            this.label1.TabIndex = 0;
            this.label1.Tag = "INSP";
            this.label1.Text = "Inspection";
            // 
            // pnlLeft
            // 
            this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlLeft.Location = new System.Drawing.Point(4, 5);
            this.pnlLeft.Name = "pnlLeft";
            this.pnlLeft.Size = new System.Drawing.Size(253, 468);
            this.pnlLeft.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.pictureBox2);
            this.tabPage6.Controls.Add(this.pnlImages);
            this.tabPage6.Location = new System.Drawing.Point(4, 29);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(1364, 478);
            this.tabPage6.TabIndex = 7;
            this.tabPage6.Text = "Photo2";
            this.tabPage6.UseVisualStyleBackColor = true;
            this.tabPage6.Click += new System.EventHandler(this.tabPage6_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.ContextMenuStrip = this.contextMenuStrip1;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Location = new System.Drawing.Point(303, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(1058, 472);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rotateToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(109, 26);
            this.contextMenuStrip1.Text = "Rotate";
            // 
            // rotateToolStripMenuItem
            // 
            this.rotateToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clockwiseToolStripMenuItem,
            this.anticlockwiseToolStripMenuItem,
            this.toolStripMenuItem2});
            this.rotateToolStripMenuItem.Name = "rotateToolStripMenuItem";
            this.rotateToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.rotateToolStripMenuItem.Text = "Rotate";
            this.rotateToolStripMenuItem.Click += new System.EventHandler(this.rotateToolStripMenuItem_Click);
            // 
            // clockwiseToolStripMenuItem
            // 
            this.clockwiseToolStripMenuItem.Name = "clockwiseToolStripMenuItem";
            this.clockwiseToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.clockwiseToolStripMenuItem.Text = "90⁰ Clockwise";
            this.clockwiseToolStripMenuItem.Click += new System.EventHandler(this.clockwiseToolStripMenuItem_Click);
            // 
            // anticlockwiseToolStripMenuItem
            // 
            this.anticlockwiseToolStripMenuItem.Name = "anticlockwiseToolStripMenuItem";
            this.anticlockwiseToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.anticlockwiseToolStripMenuItem.Text = "90⁰ Anticlockwise";
            this.anticlockwiseToolStripMenuItem.Click += new System.EventHandler(this.anticlockwiseToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(166, 22);
            this.toolStripMenuItem2.Text = "180⁰";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // pnlImages
            // 
            this.pnlImages.AutoScroll = true;
            this.pnlImages.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlImages.Location = new System.Drawing.Point(3, 3);
            this.pnlImages.Name = "pnlImages";
            this.pnlImages.Size = new System.Drawing.Size(300, 472);
            this.pnlImages.TabIndex = 0;
            // 
            // tabPage7
            // 
            this.tabPage7.Location = new System.Drawing.Point(4, 29);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(1364, 478);
            this.tabPage7.TabIndex = 8;
            this.tabPage7.Text = "VOR/BOR";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // tpBreakdown
            // 
            this.tpBreakdown.Location = new System.Drawing.Point(4, 29);
            this.tpBreakdown.Name = "tpBreakdown";
            this.tpBreakdown.Padding = new System.Windows.Forms.Padding(3);
            this.tpBreakdown.Size = new System.Drawing.Size(1364, 478);
            this.tpBreakdown.TabIndex = 9;
            this.tpBreakdown.Text = "Breakdown";
            this.tpBreakdown.UseVisualStyleBackColor = true;
            // 
            // ramsdevDataSet3
            // 
            this.ramsdevDataSet3.DataSetName = "ramsdevDataSet3";
            this.ramsdevDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uspSMARTgetVehiclesForCustomerBindingSource
            // 
            this.uspSMARTgetVehiclesForCustomerBindingSource.DataMember = "usp_SMART_getVehiclesForCustomer";
            this.uspSMARTgetVehiclesForCustomerBindingSource.DataSource = this.ramsdevDataSet3;
            // 
            // usp_SMART_getVehiclesForCustomerTableAdapter
            // 
            this.usp_SMART_getVehiclesForCustomerTableAdapter.ClearBeforeFill = true;
            // 
            // smartdevDataSet3
            // 
            this.smartdevDataSet3.DataSetName = "smartdevDataSet3";
            this.smartdevDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // uspSMARTgetVehicleDetailsBindingSource
            // 
            this.uspSMARTgetVehicleDetailsBindingSource.DataMember = "usp_SMARTgetVehicleDetails";
            this.uspSMARTgetVehicleDetailsBindingSource.DataSource = this.smartdevDataSet3;
            // 
            // usp_SMARTgetVehicleDetailsTableAdapter
            // 
            this.usp_SMARTgetVehicleDetailsTableAdapter.ClearBeforeFill = true;
            // 
            // usp_SMARTgetAllVehiclesTableAdapter
            // 
            this.usp_SMARTgetAllVehiclesTableAdapter.ClearBeforeFill = true;
            // 
            // frmMaint
            // 
            this.Appearance.Options.UseFont = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1372, 665);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pnlHeader);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMaint";
            this.Text = "frmMaint";
            this.Load += new System.EventHandler(this.frmMaint_Load);
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetAllVehiclesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet4)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tpDocs.ResumeLayout(false);
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ramsdevDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetVehiclesForCustomerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetVehicleDetailsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tpDocs;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlLeft;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.DateTimePicker dtpMOT;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Panel pnlImages;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem rotateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clockwiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem anticlockwiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.Label label6;
        private ramsdevDataSet3 ramsdevDataSet3;
        private System.Windows.Forms.BindingSource uspSMARTgetVehiclesForCustomerBindingSource;
        private ramsdevDataSet3TableAdapters.usp_SMART_getVehiclesForCustomerTableAdapter usp_SMART_getVehiclesForCustomerTableAdapter;
        private System.Windows.Forms.ComboBox cboReg;
        private System.Windows.Forms.BindingSource uspSMARTgetVehicleDetailsBindingSource;
        private smartdevDataSet3 smartdevDataSet3;
        private smartdevDataSet3TableAdapters.usp_SMARTgetVehicleDetailsTableAdapter usp_SMARTgetVehicleDetailsTableAdapter;
        private smartdevDataSet4 smartdevDataSet4;
        private System.Windows.Forms.BindingSource uspSMARTgetAllVehiclesBindingSource;
        private smartdevDataSet4TableAdapters.usp_SMARTgetAllVehiclesTableAdapter usp_SMARTgetAllVehiclesTableAdapter;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.DateTimePicker dateTimePicker10;
        private System.Windows.Forms.DateTimePicker dateTimePicker9;
        private System.Windows.Forms.DateTimePicker dateTimePicker8;
        private System.Windows.Forms.DateTimePicker dateTimePicker7;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TabPage tpBreakdown;
    }
}