﻿namespace Smart
{
    partial class frmStock
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvStockHistory = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.tbTyreSizeSearch = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboCostCentre = new System.Windows.Forms.ComboBox();
            this.uspSMARTgetCostCentresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.smartdevDataSet1 = new Smart.smartdevDataSet1();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpCurrent = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.productrefDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productdescriptionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productSizeShortDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costpriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.custpriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costCentreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.centreDescDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.homeCostCentreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.averagecostDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uspSMARTgetCurrentStockBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.uspGetCurrentStock = new Smart.uspGetCurrentStock();
            this.tpHistory = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.productDescriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.centreDescDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceProviderLocNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.quantityDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.costDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datetimeAddedDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.actionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jobNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockTransferIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productSizeShortDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.uspSMARTstockHistoryBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.uspSMARTstockHistoryBindingSource = new Smart.uspSMARTstockHistoryBindingSource();
            this.tpAddReturn = new System.Windows.Forms.TabPage();
            this.tpTransfer = new System.Windows.Forms.TabPage();
            this.tpReports = new System.Windows.Forms.TabPage();
            this.smartdevDataSet2 = new Smart.smartdevDataSet2();
            this.usp_SMART_getCostCentresTableAdapter = new Smart.smartdevDataSet1TableAdapters.usp_SMART_getCostCentresTableAdapter();
            this.usp_SMART_stockHistoryTableAdapter = new Smart.smartdevDataSet2TableAdapters.usp_SMART_stockHistoryTableAdapter();
            this.usp_SMART_stockHistoryTableAdapter1 = new Smart.uspSMARTstockHistoryBindingSourceTableAdapters.usp_SMART_stockHistoryTableAdapter();
            this.usp_SMART_getCurrentStockTableAdapter = new Smart.uspGetCurrentStockTableAdapters.usp_SMART_getCurrentStockTableAdapter();
            this.dgvStockHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCostCentresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet1)).BeginInit();
            this.pnlMain.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpCurrent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCurrentStockBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspGetCurrentStock)).BeginInit();
            this.tpHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTstockHistoryBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTstockHistoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet2)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvStockHistory
            // 
            this.dgvStockHistory.Controls.Add(this.button1);
            this.dgvStockHistory.Controls.Add(this.tbTyreSizeSearch);
            this.dgvStockHistory.Controls.Add(this.label2);
            this.dgvStockHistory.Controls.Add(this.cboCostCentre);
            this.dgvStockHistory.Controls.Add(this.label1);
            this.dgvStockHistory.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvStockHistory.Location = new System.Drawing.Point(0, 0);
            this.dgvStockHistory.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvStockHistory.Name = "dgvStockHistory";
            this.dgvStockHistory.Size = new System.Drawing.Size(1599, 88);
            this.dgvStockHistory.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(988, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 25);
            this.button1.TabIndex = 4;
            this.button1.Text = "Search";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbTyreSizeSearch
            // 
            this.tbTyreSizeSearch.Location = new System.Drawing.Point(579, 28);
            this.tbTyreSizeSearch.Name = "tbTyreSizeSearch";
            this.tbTyreSizeSearch.Size = new System.Drawing.Size(333, 26);
            this.tbTyreSizeSearch.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(498, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tyre Size";
            // 
            // cboCostCentre
            // 
            this.cboCostCentre.DataSource = this.uspSMARTgetCostCentresBindingSource;
            this.cboCostCentre.DisplayMember = "Centre_Desc";
            this.cboCostCentre.FormattingEnabled = true;
            this.cboCostCentre.Location = new System.Drawing.Point(197, 26);
            this.cboCostCentre.Name = "cboCostCentre";
            this.cboCostCentre.Size = new System.Drawing.Size(225, 28);
            this.cboCostCentre.TabIndex = 1;
            this.cboCostCentre.ValueMember = "CentreID";
            this.cboCostCentre.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // uspSMARTgetCostCentresBindingSource
            // 
            this.uspSMARTgetCostCentresBindingSource.DataMember = "usp_SMART_getCostCentres";
            this.uspSMARTgetCostCentresBindingSource.DataSource = this.smartdevDataSet1;
            // 
            // smartdevDataSet1
            // 
            this.smartdevDataSet1.DataSetName = "smartdevDataSet1";
            this.smartdevDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(48, 29);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Depot/Cost Centre";
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.tabControl1);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 88);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(1599, 810);
            this.pnlMain.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpCurrent);
            this.tabControl1.Controls.Add(this.tpHistory);
            this.tabControl1.Controls.Add(this.tpAddReturn);
            this.tabControl1.Controls.Add(this.tpTransfer);
            this.tabControl1.Controls.Add(this.tpReports);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1599, 810);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpCurrent
            // 
            this.tpCurrent.Controls.Add(this.dataGridView2);
            this.tpCurrent.Location = new System.Drawing.Point(4, 33);
            this.tpCurrent.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpCurrent.Name = "tpCurrent";
            this.tpCurrent.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpCurrent.Size = new System.Drawing.Size(1591, 773);
            this.tpCurrent.TabIndex = 0;
            this.tpCurrent.Text = "Current";
            this.tpCurrent.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productrefDataGridViewTextBoxColumn,
            this.productdescriptionDataGridViewTextBoxColumn1,
            this.productSizeShortDataGridViewTextBoxColumn1,
            this.costpriceDataGridViewTextBoxColumn,
            this.custpriceDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn1,
            this.costCentreDataGridViewTextBoxColumn,
            this.centreDescDataGridViewTextBoxColumn1,
            this.homeCostCentreDataGridViewTextBoxColumn,
            this.averagecostDataGridViewTextBoxColumn});
            this.dataGridView2.DataSource = this.uspSMARTgetCurrentStockBindingSource;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(4, 5);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(1583, 763);
            this.dataGridView2.TabIndex = 0;
            // 
            // productrefDataGridViewTextBoxColumn
            // 
            this.productrefDataGridViewTextBoxColumn.DataPropertyName = "product_ref";
            this.productrefDataGridViewTextBoxColumn.HeaderText = "product_ref";
            this.productrefDataGridViewTextBoxColumn.Name = "productrefDataGridViewTextBoxColumn";
            // 
            // productdescriptionDataGridViewTextBoxColumn1
            // 
            this.productdescriptionDataGridViewTextBoxColumn1.DataPropertyName = "product_description";
            this.productdescriptionDataGridViewTextBoxColumn1.HeaderText = "product_description";
            this.productdescriptionDataGridViewTextBoxColumn1.Name = "productdescriptionDataGridViewTextBoxColumn1";
            // 
            // productSizeShortDataGridViewTextBoxColumn1
            // 
            this.productSizeShortDataGridViewTextBoxColumn1.DataPropertyName = "Product_Size_Short";
            this.productSizeShortDataGridViewTextBoxColumn1.HeaderText = "Product_Size_Short";
            this.productSizeShortDataGridViewTextBoxColumn1.Name = "productSizeShortDataGridViewTextBoxColumn1";
            // 
            // costpriceDataGridViewTextBoxColumn
            // 
            this.costpriceDataGridViewTextBoxColumn.DataPropertyName = "cost_price";
            this.costpriceDataGridViewTextBoxColumn.HeaderText = "cost_price";
            this.costpriceDataGridViewTextBoxColumn.Name = "costpriceDataGridViewTextBoxColumn";
            this.costpriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // custpriceDataGridViewTextBoxColumn
            // 
            this.custpriceDataGridViewTextBoxColumn.DataPropertyName = "cust_price";
            this.custpriceDataGridViewTextBoxColumn.HeaderText = "cust_price";
            this.custpriceDataGridViewTextBoxColumn.Name = "custpriceDataGridViewTextBoxColumn";
            this.custpriceDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // quantityDataGridViewTextBoxColumn1
            // 
            this.quantityDataGridViewTextBoxColumn1.DataPropertyName = "quantity";
            this.quantityDataGridViewTextBoxColumn1.HeaderText = "quantity";
            this.quantityDataGridViewTextBoxColumn1.Name = "quantityDataGridViewTextBoxColumn1";
            // 
            // costCentreDataGridViewTextBoxColumn
            // 
            this.costCentreDataGridViewTextBoxColumn.DataPropertyName = "costCentre";
            this.costCentreDataGridViewTextBoxColumn.HeaderText = "costCentre";
            this.costCentreDataGridViewTextBoxColumn.Name = "costCentreDataGridViewTextBoxColumn";
            this.costCentreDataGridViewTextBoxColumn.Visible = false;
            // 
            // centreDescDataGridViewTextBoxColumn1
            // 
            this.centreDescDataGridViewTextBoxColumn1.DataPropertyName = "Centre_Desc";
            this.centreDescDataGridViewTextBoxColumn1.HeaderText = "Centre_Desc";
            this.centreDescDataGridViewTextBoxColumn1.Name = "centreDescDataGridViewTextBoxColumn1";
            // 
            // homeCostCentreDataGridViewTextBoxColumn
            // 
            this.homeCostCentreDataGridViewTextBoxColumn.DataPropertyName = "homeCostCentre";
            this.homeCostCentreDataGridViewTextBoxColumn.HeaderText = "homeCostCentre";
            this.homeCostCentreDataGridViewTextBoxColumn.Name = "homeCostCentreDataGridViewTextBoxColumn";
            this.homeCostCentreDataGridViewTextBoxColumn.ReadOnly = true;
            this.homeCostCentreDataGridViewTextBoxColumn.Visible = false;
            // 
            // averagecostDataGridViewTextBoxColumn
            // 
            this.averagecostDataGridViewTextBoxColumn.DataPropertyName = "averagecost";
            this.averagecostDataGridViewTextBoxColumn.HeaderText = "averagecost";
            this.averagecostDataGridViewTextBoxColumn.Name = "averagecostDataGridViewTextBoxColumn";
            // 
            // uspSMARTgetCurrentStockBindingSource
            // 
            this.uspSMARTgetCurrentStockBindingSource.DataMember = "usp_SMART_getCurrentStock";
            this.uspSMARTgetCurrentStockBindingSource.DataSource = this.uspGetCurrentStock;
            // 
            // uspGetCurrentStock
            // 
            this.uspGetCurrentStock.DataSetName = "uspGetCurrentStock";
            this.uspGetCurrentStock.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tpHistory
            // 
            this.tpHistory.Controls.Add(this.dataGridView1);
            this.tpHistory.Location = new System.Drawing.Point(4, 33);
            this.tpHistory.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpHistory.Name = "tpHistory";
            this.tpHistory.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpHistory.Size = new System.Drawing.Size(1591, 773);
            this.tpHistory.TabIndex = 1;
            this.tpHistory.Text = "History";
            this.tpHistory.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productDescriptionDataGridViewTextBoxColumn,
            this.centreDescDataGridViewTextBoxColumn,
            this.serviceProviderLocNameDataGridViewTextBoxColumn,
            this.quantityDataGridViewTextBoxColumn,
            this.costDataGridViewTextBoxColumn,
            this.datetimeAddedDataGridViewTextBoxColumn,
            this.actionDataGridViewTextBoxColumn,
            this.jobNumberDataGridViewTextBoxColumn,
            this.stockTransferIDDataGridViewTextBoxColumn,
            this.productSizeShortDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.uspSMARTstockHistoryBindingSource1;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(4, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1583, 763);
            this.dataGridView1.TabIndex = 0;
            // 
            // productDescriptionDataGridViewTextBoxColumn
            // 
            this.productDescriptionDataGridViewTextBoxColumn.DataPropertyName = "Product_Description";
            this.productDescriptionDataGridViewTextBoxColumn.HeaderText = "Product_Description";
            this.productDescriptionDataGridViewTextBoxColumn.Name = "productDescriptionDataGridViewTextBoxColumn";
            // 
            // centreDescDataGridViewTextBoxColumn
            // 
            this.centreDescDataGridViewTextBoxColumn.DataPropertyName = "Centre_Desc";
            this.centreDescDataGridViewTextBoxColumn.HeaderText = "Centre_Desc";
            this.centreDescDataGridViewTextBoxColumn.Name = "centreDescDataGridViewTextBoxColumn";
            // 
            // serviceProviderLocNameDataGridViewTextBoxColumn
            // 
            this.serviceProviderLocNameDataGridViewTextBoxColumn.DataPropertyName = "Service_Provider_Loc_Name";
            this.serviceProviderLocNameDataGridViewTextBoxColumn.FillWeight = 140F;
            this.serviceProviderLocNameDataGridViewTextBoxColumn.HeaderText = "Service_Provider_Loc_Name";
            this.serviceProviderLocNameDataGridViewTextBoxColumn.Name = "serviceProviderLocNameDataGridViewTextBoxColumn";
            // 
            // quantityDataGridViewTextBoxColumn
            // 
            this.quantityDataGridViewTextBoxColumn.DataPropertyName = "quantity";
            this.quantityDataGridViewTextBoxColumn.HeaderText = "quantity";
            this.quantityDataGridViewTextBoxColumn.Name = "quantityDataGridViewTextBoxColumn";
            // 
            // costDataGridViewTextBoxColumn
            // 
            this.costDataGridViewTextBoxColumn.DataPropertyName = "cost";
            this.costDataGridViewTextBoxColumn.HeaderText = "cost";
            this.costDataGridViewTextBoxColumn.Name = "costDataGridViewTextBoxColumn";
            // 
            // datetimeAddedDataGridViewTextBoxColumn
            // 
            this.datetimeAddedDataGridViewTextBoxColumn.DataPropertyName = "datetimeAdded";
            this.datetimeAddedDataGridViewTextBoxColumn.HeaderText = "datetimeAdded";
            this.datetimeAddedDataGridViewTextBoxColumn.Name = "datetimeAddedDataGridViewTextBoxColumn";
            // 
            // actionDataGridViewTextBoxColumn
            // 
            this.actionDataGridViewTextBoxColumn.DataPropertyName = "Action";
            this.actionDataGridViewTextBoxColumn.HeaderText = "Action";
            this.actionDataGridViewTextBoxColumn.Name = "actionDataGridViewTextBoxColumn";
            this.actionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // jobNumberDataGridViewTextBoxColumn
            // 
            this.jobNumberDataGridViewTextBoxColumn.DataPropertyName = "JobNumber";
            this.jobNumberDataGridViewTextBoxColumn.HeaderText = "JobNumber";
            this.jobNumberDataGridViewTextBoxColumn.Name = "jobNumberDataGridViewTextBoxColumn";
            // 
            // stockTransferIDDataGridViewTextBoxColumn
            // 
            this.stockTransferIDDataGridViewTextBoxColumn.DataPropertyName = "stockTransferID";
            this.stockTransferIDDataGridViewTextBoxColumn.HeaderText = "stockTransferID";
            this.stockTransferIDDataGridViewTextBoxColumn.Name = "stockTransferIDDataGridViewTextBoxColumn";
            // 
            // productSizeShortDataGridViewTextBoxColumn
            // 
            this.productSizeShortDataGridViewTextBoxColumn.DataPropertyName = "Product_Size_Short";
            this.productSizeShortDataGridViewTextBoxColumn.HeaderText = "Product_Size_Short";
            this.productSizeShortDataGridViewTextBoxColumn.Name = "productSizeShortDataGridViewTextBoxColumn";
            this.productSizeShortDataGridViewTextBoxColumn.Visible = false;
            // 
            // uspSMARTstockHistoryBindingSource1
            // 
            this.uspSMARTstockHistoryBindingSource1.DataMember = "usp_SMART_stockHistory";
            this.uspSMARTstockHistoryBindingSource1.DataSource = this.uspSMARTstockHistoryBindingSource;
            // 
            // uspSMARTstockHistoryBindingSource
            // 
            this.uspSMARTstockHistoryBindingSource.DataSetName = "uspSMARTstockHistoryBindingSource";
            this.uspSMARTstockHistoryBindingSource.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tpAddReturn
            // 
            this.tpAddReturn.Location = new System.Drawing.Point(4, 33);
            this.tpAddReturn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpAddReturn.Name = "tpAddReturn";
            this.tpAddReturn.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpAddReturn.Size = new System.Drawing.Size(1591, 773);
            this.tpAddReturn.TabIndex = 2;
            this.tpAddReturn.Text = "Add/Return";
            this.tpAddReturn.UseVisualStyleBackColor = true;
            // 
            // tpTransfer
            // 
            this.tpTransfer.Location = new System.Drawing.Point(4, 33);
            this.tpTransfer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpTransfer.Name = "tpTransfer";
            this.tpTransfer.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpTransfer.Size = new System.Drawing.Size(1591, 773);
            this.tpTransfer.TabIndex = 3;
            this.tpTransfer.Text = "Transfer";
            this.tpTransfer.UseVisualStyleBackColor = true;
            // 
            // tpReports
            // 
            this.tpReports.Location = new System.Drawing.Point(4, 33);
            this.tpReports.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpReports.Name = "tpReports";
            this.tpReports.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpReports.Size = new System.Drawing.Size(1591, 773);
            this.tpReports.TabIndex = 4;
            this.tpReports.Text = "Reports";
            this.tpReports.UseVisualStyleBackColor = true;
            // 
            // smartdevDataSet2
            // 
            this.smartdevDataSet2.DataSetName = "smartdevDataSet2";
            this.smartdevDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // usp_SMART_getCostCentresTableAdapter
            // 
            this.usp_SMART_getCostCentresTableAdapter.ClearBeforeFill = true;
            // 
            // usp_SMART_stockHistoryTableAdapter
            // 
            this.usp_SMART_stockHistoryTableAdapter.ClearBeforeFill = true;
            // 
            // usp_SMART_stockHistoryTableAdapter1
            // 
            this.usp_SMART_stockHistoryTableAdapter1.ClearBeforeFill = true;
            // 
            // usp_SMART_getCurrentStockTableAdapter
            // 
            this.usp_SMART_getCurrentStockTableAdapter.ClearBeforeFill = true;
            // 
            // frmStock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(1599, 898);
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.dgvStockHistory);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmStock";
            this.Text = "frmStock";
            this.Load += new System.EventHandler(this.frmStock_Load);
            this.dgvStockHistory.ResumeLayout(false);
            this.dgvStockHistory.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCostCentresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet1)).EndInit();
            this.pnlMain.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpCurrent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTgetCurrentStockBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspGetCurrentStock)).EndInit();
            this.tpHistory.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTstockHistoryBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uspSMARTstockHistoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smartdevDataSet2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel dgvStockHistory;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpCurrent;
        private System.Windows.Forms.TabPage tpHistory;
        private System.Windows.Forms.TabPage tpAddReturn;
        private System.Windows.Forms.TabPage tpTransfer;
        private System.Windows.Forms.TabPage tpReports;
        private System.Windows.Forms.ComboBox cboCostCentre;
        private System.Windows.Forms.Label label1;
        private smartdevDataSet1 smartdevDataSet1;
        private System.Windows.Forms.BindingSource uspSMARTgetCostCentresBindingSource;
        private smartdevDataSet1TableAdapters.usp_SMART_getCostCentresTableAdapter usp_SMART_getCostCentresTableAdapter;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbTyreSizeSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private smartdevDataSet2 smartdevDataSet2;
        private smartdevDataSet2TableAdapters.usp_SMART_stockHistoryTableAdapter usp_SMART_stockHistoryTableAdapter;
        private uspSMARTstockHistoryBindingSource uspSMARTstockHistoryBindingSource;
        private System.Windows.Forms.BindingSource uspSMARTstockHistoryBindingSource1;
        private uspSMARTstockHistoryBindingSourceTableAdapters.usp_SMART_stockHistoryTableAdapter usp_SMART_stockHistoryTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productDescriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn centreDescDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn serviceProviderLocNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn costDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn datetimeAddedDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn actionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn jobNumberDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockTransferIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productSizeShortDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn productrefDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn productdescriptionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn productSizeShortDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn costpriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn custpriceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn quantityDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn costCentreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn centreDescDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn homeCostCentreDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn averagecostDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource uspSMARTgetCurrentStockBindingSource;
        private uspGetCurrentStock uspGetCurrentStock;
        private uspGetCurrentStockTableAdapters.usp_SMART_getCurrentStockTableAdapter usp_SMART_getCurrentStockTableAdapter;
    }
}