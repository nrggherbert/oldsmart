﻿namespace Smart
{
    partial class frmCompliance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler3 = new DevExpress.XtraScheduler.TimeRuler();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpSchedule = new System.Windows.Forms.TabPage();
            this.calendar1 = new Calendar.NET.Calendar();
            this.tpOverview = new System.Windows.Forms.TabPage();
            this.schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerStorage();
            this.tpCustomerVisit = new System.Windows.Forms.TabPage();
            this.usp_SMART_getCustomerLocationsTableAdapter1 = new Smart.smartdevDataSet6TableAdapters.usp_SMART_getCustomerLocationsTableAdapter();
            this.dsVehicleDates = new Smart.dsVehicleDates();
            this.vwvehicleDatesBindingSource = new System.Windows.Forms.BindingSource();
            this.vw_vehicleDatesTableAdapter = new Smart.dsVehicleDatesTableAdapters.vw_vehicleDatesTableAdapter();
            this.schedulerControl1 = new DevExpress.XtraScheduler.SchedulerControl();
            this.tabControl1.SuspendLayout();
            this.tpSchedule.SuspendLayout();
            this.tpOverview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicleDates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwvehicleDatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTop
            // 
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(1143, 154);
            this.pnlTop.TabIndex = 0;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpSchedule);
            this.tabControl1.Controls.Add(this.tpOverview);
            this.tabControl1.Controls.Add(this.tpCustomerVisit);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 154);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1143, 618);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tpSchedule
            // 
            this.tpSchedule.Controls.Add(this.calendar1);
            this.tpSchedule.Location = new System.Drawing.Point(4, 29);
            this.tpSchedule.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpSchedule.Name = "tpSchedule";
            this.tpSchedule.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpSchedule.Size = new System.Drawing.Size(1135, 585);
            this.tpSchedule.TabIndex = 0;
            this.tpSchedule.Text = "Schedule";
            this.tpSchedule.UseVisualStyleBackColor = true;
            // 
            // calendar1
            // 
            this.calendar1.AllowEditingEvents = false;
            this.calendar1.BackColor = System.Drawing.Color.Transparent;
            this.calendar1.CalendarDate = new System.DateTime(2012, 4, 24, 13, 16, 0, 0);
            this.calendar1.CalendarView = Calendar.NET.CalendarViews.Month;
            this.calendar1.DateHeaderFont = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.calendar1.DayOfWeekFont = new System.Drawing.Font("Arial", 10F);
            this.calendar1.DaysFont = new System.Drawing.Font("Arial", 10F);
            this.calendar1.DayViewTimeFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.calendar1.DimDisabledEvents = true;
            this.calendar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendar1.HighlightCurrentDay = true;
            this.calendar1.LoadPresetHolidays = true;
            this.calendar1.Location = new System.Drawing.Point(4, 5);
            this.calendar1.Name = "calendar1";
            this.calendar1.ShowArrowControls = true;
            this.calendar1.ShowDashedBorderOnDisabledEvents = true;
            this.calendar1.ShowDateInHeader = true;
            this.calendar1.ShowDisabledEvents = false;
            this.calendar1.ShowEventTooltips = true;
            this.calendar1.ShowTodayButton = true;
            this.calendar1.Size = new System.Drawing.Size(1127, 575);
            this.calendar1.TabIndex = 1;
            this.calendar1.TodayFont = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            // 
            // tpOverview
            // 
            this.tpOverview.Controls.Add(this.schedulerControl1);
            this.tpOverview.Location = new System.Drawing.Point(4, 29);
            this.tpOverview.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpOverview.Name = "tpOverview";
            this.tpOverview.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tpOverview.Size = new System.Drawing.Size(1135, 585);
            this.tpOverview.TabIndex = 1;
            this.tpOverview.Text = "Overview";
            this.tpOverview.UseVisualStyleBackColor = true;
            // 
            // schedulerStorage1
            // 
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("AServicePeriod", "A_Service_Period"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("AServiceValue", "A_Service_Value"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("BServicePeriod", "B_Service_Period"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("BServiceValue", "B_Service_Value"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("BinLiftDue", "BinLift_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("Body52Due", "Body_52_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("BodyWarrentyExpiryDate", "Body_Warrenty_Expiry_Date"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("BrakeTestDue", "BrakeTest_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("CServicePeriod", "C_Service_Period"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("CServiceValue", "C_Service_Value"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("CompressorDue", "Compressor_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("DServicePeriod", "D_Service_Period"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("DServiceValue", "D_Service_Value"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("DoubleDeck12Due", "DoubleDeck12_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("DoubleDeck6Due", "DoubleDeck6_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("DPFDue", "DPF_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("FridgeGasCertDue", "FridgeGasCert_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("FridgeServiceDue", "FridgeService_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("InitialOdometerReading", "Initial_Odometer_Reading"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("InitialOdometerReadingDate", "Initial_Odometer_Reading_Date"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("InspectionDue", "Inspection_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("LecExpiryDate", "Lec_Expiry_Date"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("LiftWarrentyExpiryDate", "Lift_Warrenty_Expiry_Date"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("Loler12", "Loler_12"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("Loler6", "Loler_6"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("LOLERMonthlyInterval", "LOLER_Monthly_Interval"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("LOLERServiceDue", "LOLER_Service_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("Mileage", "Mileage"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("MOTDue", "MOT_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("OdometerTypeID", "Odometer_Type_ID"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("RoadTaxRenewalDate", "Road_Tax_Renewal_Date"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("RPCDue", "RPC_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("ServiceADue", "Service_A_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("ServiceBDue", "Service_B_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("ServiceCDue", "Service_C_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("ServiceDDue", "Service_D_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("ShuttersDue", "Shutters_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("SWLDue", "SWL_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("Tacho2Due", "Tacho2_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("Tacho6Due", "Tacho6_due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("TailLift12Due", "TailLift12_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("TailLift6Due", "TailLift6_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("TailLiftWeight12Due", "TailLiftWeight12_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("TailLiftWeight6Due", "TailLiftWeight6_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("TankPressureCertDue", "TankPressureCert_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("VBGCouplingDue", "VBGCoupling_Due"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("VehicleInsuranceID", "Vehicle_Insurance_ID"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("VehicleOwnershipID", "Vehicle_Ownership_ID"));
            this.schedulerStorage1.AppointmentDependencies.CustomFieldMappings.Add(new DevExpress.XtraScheduler.AppointmentDependencyCustomFieldMapping("WarrantyExpiryDate", "Warranty_Expiry_Date"));
            this.schedulerStorage1.AppointmentDependencies.DataSource = this.vwvehicleDatesBindingSource;
            this.schedulerStorage1.AppointmentDependencies.Mappings.DependentId = "Vehicle_Reg";
            this.schedulerStorage1.AppointmentDependencies.Mappings.ParentId = "Fleetlist_Id";
            this.schedulerStorage1.AppointmentDependencies.Mappings.Type = "AnnualTest_due";
            // 
            // tpCustomerVisit
            // 
            this.tpCustomerVisit.Location = new System.Drawing.Point(4, 29);
            this.tpCustomerVisit.Name = "tpCustomerVisit";
            this.tpCustomerVisit.Padding = new System.Windows.Forms.Padding(3);
            this.tpCustomerVisit.Size = new System.Drawing.Size(1135, 585);
            this.tpCustomerVisit.TabIndex = 2;
            this.tpCustomerVisit.Text = "Customer Visits";
            this.tpCustomerVisit.UseVisualStyleBackColor = true;
            // 
            // usp_SMART_getCustomerLocationsTableAdapter1
            // 
            this.usp_SMART_getCustomerLocationsTableAdapter1.ClearBeforeFill = true;
            // 
            // dsVehicleDates
            // 
            this.dsVehicleDates.DataSetName = "dsVehicleDates";
            this.dsVehicleDates.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // vwvehicleDatesBindingSource
            // 
            this.vwvehicleDatesBindingSource.DataMember = "vw_vehicleDates";
            this.vwvehicleDatesBindingSource.DataSource = this.dsVehicleDates;
            // 
            // vw_vehicleDatesTableAdapter
            // 
            this.vw_vehicleDatesTableAdapter.ClearBeforeFill = true;
            // 
            // schedulerControl1
            // 
            this.schedulerControl1.DataStorage = this.schedulerStorage1;
            this.schedulerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.schedulerControl1.Location = new System.Drawing.Point(4, 5);
            this.schedulerControl1.Name = "schedulerControl1";
            this.schedulerControl1.Size = new System.Drawing.Size(1127, 575);
            this.schedulerControl1.Start = new System.DateTime(2017, 8, 2, 0, 0, 0, 0);
            this.schedulerControl1.TabIndex = 0;
            this.schedulerControl1.Text = "schedulerControl1";
            this.schedulerControl1.Views.DayView.TimeRulers.Add(timeRuler1);
            this.schedulerControl1.Views.FullWeekView.Enabled = true;
            this.schedulerControl1.Views.FullWeekView.TimeRulers.Add(timeRuler2);
            this.schedulerControl1.Views.WeekView.Enabled = false;
            this.schedulerControl1.Views.WorkWeekView.TimeRulers.Add(timeRuler3);
            // 
            // frmCompliance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(1143, 772);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.pnlTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmCompliance";
            this.Text = "frmCompliance";
            this.Load += new System.EventHandler(this.frmCompliance_Load);
            this.tabControl1.ResumeLayout(false);
            this.tpSchedule.ResumeLayout(false);
            this.tpOverview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.schedulerStorage1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsVehicleDates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwvehicleDatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.schedulerControl1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpSchedule;
        private System.Windows.Forms.TabPage tpOverview;
        private System.Windows.Forms.TabPage tpCustomerVisit;
        private Calendar.NET.Calendar calendar1;
        private smartdevDataSet6TableAdapters.usp_SMART_getCustomerLocationsTableAdapter usp_SMART_getCustomerLocationsTableAdapter1;
        private DevExpress.XtraEditors.Controls.CalendarControl calendarControl1;
        private DevExpress.XtraScheduler.SchedulerStorage schedulerStorage1;
        private dsVehicleDates dsVehicleDates;
        private System.Windows.Forms.BindingSource vwvehicleDatesBindingSource;
        private dsVehicleDatesTableAdapters.vw_vehicleDatesTableAdapter vw_vehicleDatesTableAdapter;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControl1;
    }
}