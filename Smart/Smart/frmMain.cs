﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Smart
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ramsdevDataSet.usp_SMART_activeBreakdowns' table. You can move, or remove it, as needed.
            this.usp_SMART_activeBreakdownsTableAdapter.Fill(this.ramsdevDataSet.usp_SMART_activeBreakdowns);
            lblWelcome.Text = "Welcome " + Environment.UserName + ".  Here are your messages for today";

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
